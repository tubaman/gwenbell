---
title: The Handcrafted List
date: 2013-12-10
---

I dedicate this piece to Sougwen, the first person to send me a note to get onto the handcrafted list. [Visit her site if you wish](http://sougwen.com). As for the coincidental similarity between our names, she said, "My name is actually an antiquated phonetization of my chinese name 愫君 pronounced (soo-gwen)." She also sent this note: "[Your entry referencing Synedoche](/buyin) reminded me of '[On Self Respect](http://profacero.wordpress.com/2010/11/29/joan-didion-on-self-respect)' an essay by Joan Didion." Which reminds me to add _The Year of Magical Thinking_ to [Appreciations](/appreciations). It's a pleasure to slow meet you, Sougwen.

***

This morning I ate almost an entire pineapple. 

Salt, lime juice, pineapple. It ran all over the place; the right balance of sweet, sticky and salty.

Before I ate the pineapple, I got a message from a new reader. 

I am not sure how she found my work, only that she found it, only that something about it landed for her. She saw that (as of yesterday) I no longer have a subscribe to updates button on my site. If you'd like to get on the list, you have to email me and say please.

Not only did she email to say please, she said she'd never subscribed to anyone's list by contacting the writer. Maybe it felt novel to her. After I publish this, I'm going to email her back, tell her about what I noticed on her site, and ask her a question.

That is what I mean by the handcrafted list.

#### Slower, Slower

I want connections that are slower. I do not want drive-by subscribers, so if you want on the list, we have to have some connection.

Connection, actual connection, between writers and their readers, is rare. 

I rarely see what I perceive to be genuine connection on the web right now. 
I see it in IRC sometimes, in a few channels, including [my own](/gaman).

When you email me and I respond with a letter you know I've seen your face. You know that I've clicked over to your site. You know that my response is [deliberate](/deliberate).

#### Who Has Time for That?

If you ask Who Has Time? My question to you is: if you _don't_ have time for the people who read your work, why are you doing it? 

If you're writing just to get things "off your chest," why not write to a journal with a pen instead? 

If you're not writing to serve others, you're writing to serve only yourself. Commit it to a notebook instead.

If you put up a "capture" you're no better than the Bruegger's Bagel shop collecting business cards to give away a monthly bagel lunch. (By the way, do you know anyone whose ever won anything dropping their business card in a fish bowl?)

#### Two Way Street

I have always seen the work I do as a two way street. 

I don't see it as a chance to stand up in the middle of a crowded room, plug my fingers in my ears and yell. It's why I left FaceCrack in 2010. It's why I discontinued use of Twxxter in 2013. The yelling into a crowded room is a less appropriate metaphor now. 

Now you're shouting at an empty room.

#### Because I Care

I write because I care. I have always written because I care.

I publish because I care. 

I ask for you to subscribe because I think it's a convenience for you to receive an occasional message letting you know what's new. 

I ask that you get in touch with me first so we can connect. I want to know what's up in your life. I want _more_ than just _you_ to know what's up in _mine_.

That is why I have a handcrafted list. And I [invite you along](mailto:gwen@gwenbell.com) if the desire is in you.

***

> Nonetheless, character - the willingness to accept responsibility for one's own life - is the source from which self-respect springs. Self-respect is something that our grandparents, whether or not they had it, knew all about. They had instilled in them, young, a certain discipline, the sense that one lives by doing things one does not particularly want to do, by putting fears and doubts to one side, by weighing immediate comforts against the possibility of larger, even intangible, comforts. - Joan Didion, On Self-Respect
