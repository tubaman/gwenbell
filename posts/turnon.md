---
title: Ah Man This Piece is Gonna Get Me in Trouble
date: 2014-01-30
---

**(If we're related, please don't read this piece. Thanks!)**	 

<img src="/images/artopening.jpg" class="profile" style="width: 100%" />

I bought three new pairs of underwear this afternoon. Black, cotton. Microshorts. Somewhere between a thong and boy shorts.

Then I had a conversation about things that turn me on. 

Then I took a shower with my fresh bottle of Dr Bronners (thankthelordhallelujahifoundyoudrbronners) and got clean for the first time in four months.

How come I'm telling you this now? 

**Because there's so much out there right now -- in the world of tech, the only world I know -- that turns me off.** 

And I just deleted every piece on this site that's not turning me on. And I will never visit another Tumblr blog again because those turn me off. And when I see blahblah@gmail.com I will be slower to respond.

Because Google? Turns me off. All of it. I see no saving graces in Google. I get what people are so pissed about Google for. Drains me to even type the word.

Tracking? TURN OFF. I don't track visitors to this site. No adwords nonsense. You want to let me know you're lurking? [Contact me](/contact).

What turns me on? Here's some lists.

#### Turn Ons

+ Art openings
+ The weight of my new paint brush
+ The feel of my new microshorts
+ So Low, Keys N Krates
+ Frog pose
+ Saschienne
+ Rick Ross
+ Meek Mill
+ Ciara in **Like a Boy** and **Ride**
+ Future, Ciara's baby daddy, in **Honest**
+ Trap Hop
+ Pig tacos
+ Avocado
+ My hair growing out all tendrilly
+ That leaf I saw today, the perfect specimin of leaf, three spider strands
+ Ev Bogue
+ Justin Orvis Steimer
+ Zed Shaw
+ Mexico
+ Mercados
+ Sage the Gemini
+ The mountains at sunset

#### Tech Turn Ons

+ xmonad
+ Arch
+ Git
+ Haskell
+ Hakyll
+ LamerNews
+ IRC
+ systemd
+ ARCH, y'all. Arch.
+ The Command Line
+ Tor, for its identity obscuring
+ [Top ten](/tentools)

Mmk. Now. You know too much. That's what's turning me on as we come into the last day of the month. I am deleting everything from this site that doesn't turn me on. I am only publishing pieces in the future that turn me on. And I only want to hear from you if you think you can turn me on. (Unless you're family. You're exempt, which I hope is obvious. Didn't I **tell you** at the top not to read this? Ok. I don't listen to you either.)	 

#### If

If this piece turns you on, TELL SOMEONE with your MOUTH. Don't email them about it. Please don't frakin twxxt it. And definitely don't share it to some horrible wretched website that takes and takes and will never be a return on your time/attention/life investment.

Your mouth. Your words. Your hands.

Use them.

Say, "I read this piece by Gwen Bell and goddamn is she pissed off about some thing or another but she told me to have this conversation face to face and now here we are. Hey, you're cute. I just realized that. Shit. This was her plan all along, wasn't it?"

Speaking of. 

[Compersion](/compersion).
