---
title: Start Here: Gwen Bell's How Do I Even Get Started with All This Tech Stuff FAQ!!!
date: 2014-02-07
---

<img src="/images/green-and-glowing.jpg" class="profile" size="100%">

You don't get any of this "tech stuff." I hear you. You want to know where to start.

**Start with what you know.**

I am a pretty terrible [Spanish](/spanish) speaker. I should be embarrassed. But I'm not! The way I improve is by speaking TERRIBLE Spanish. That's how I learned Japanese. Watching Naruto and speaking TERRIBLE Japanese.

So when I get a message saying, "I don't even know where to START with this tech stuff you're writing about Gwen Bell!!!" I say, "start." 

Start with what you know, move to what you don't. 

#### You know how to communicate with your words. 

And you want your words to be safer through cryptography, start with [Git](http://git.gwenbell.com).

#### You know how to log in to a messaging app.

And you'd prefer your chats not be logged by a company that will then turn around and serve you ads based on your conversations. Creepy. Start with [IRC](/irc).

#### You're sick of your operating system and you wanna pull an act of violence a la [this kid](http://www.youtube.com/watch?v=DuQZTAJ2KLk) on your laptop/phone.

Start by deleting as many devices as you can. With or without a semiautomatic. Consolidate. Make hard choices. Then you might try [Arch Linux](/arch). I'd recommend Ubuntu, having been there. But can't. Because I think it's back sliding --- and I wanna hold your feet to the fire here.

#### You want to get off the social networks, but they've become a home to you.

Nope, they haven't. They answer to their shareholders. Unless you're a shareholder, they don't answer to you. Here's what you **can** do though. You can build [your own digital home](http://align.gwenbell.com), one [brick](/approach) at a time.

#### You're paranoid about being watched when you duckduckgo about that odd pain you've been feeling in your right pinkie toe.

There's Tor for that. That's a download, and some habit changes. But it bounces your IP address all over the globe and masks your identity, if done right. NOTE: If done right. If done wrong? Well, just do it right.

#### You want to start honoring other people's rights to privacy?

There's a behavior change for that. Ask before taking a photo. Leave your phone at home, because everyone knows it's monitoring every conversation now. Protect other people's privacy by first editing your own behaviors.

#### You want to live in a country that'll protect your freedoms?

Move to a country that'll do a better job than the one you're in. I realize that's a tossup at the moment. But I believe the dust will settle. And I'm not sure the country in which I was born will come out smelling roses. I [took action](/fourlocks) and got out with my life.

#### You want to spend time with others doing the work, as hard as the work is now.

Choose your mates with care. Do you want to be sharing space with people [bare backing the internet](http://motherboard.vice.com/blog/jacob-appelbaum-utopia-interview)? No? Then, don't.

#### Do it terrible

Speak bad Spanish. Speak bad Japanese. Speak bad tech.

Ask terrible questions in IRC. Or on [Twister](/twister). 

#### Ask more questions!

That's the other thing. All questions move the ball down the court. A closed mind that says, "oh I've seen this all before and I am just going to sit back and watch everybody else figure out this decentralized web thing," does not. 

Start with what you know, move to what you don't.

If you're a yoga practitioner, you know this. Your first down dog was embarrassing. You've forgotten now how embarrassed you were. Now that you can down dog in your sleep.
If you're a traveler, you know this. Your first time leaving a country with two rolling suitcases was embarrassing. You've forgotten now how embarrassed you were. Now that you live out of a single bag.
If you're a chef, you know this. Your first omelette was a mess. You've forgotten what a mess it was, now that you've mastered the Pierre the Chef or whatever his name is's way of making an omelette.

If you don't go in, make some [mistakes](/mistakes) and try again, how will you grow?
