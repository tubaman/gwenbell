---
title: After The Fire
date: 2007-05-31
---

<img src="/images/macbook-after-fire.jpg" class="profile" style="width: 100%" />

Wired picked up this story for [Melted Macbooks Look Like Fudge](http://www.wired.com/gadgetlab/2007/06/melted_macbooks/). Read it to see some of the photographic evidence I took. I donated full-sized framed photos of the fire to the Boulder County Fire Department.

#### Isa & Isobel 

Isa snarls, low and mean, as I approach the makeshift fence "keeping" her in the backyard of our former house. Her barking awoke the neighbors, resulting in a phone call to my number around 10 P.M. I drove over to rescue her before the Animal Control folks got called in. My former next door neighbor, a veterinarian, mentioned that she hesitated to alert NACA because it would, "traumatize her even more" when they pulled out the capture devices. So here I am, breathing slowly and exhaling completely, the smell of burnt plastic again clings to my nostril hairs. This time, danker and haunting. I look at my watch. It's three weeks to the night since a plastic candelabra mounted to the wall began to sag under the heat of the candle left burning in it. And our housemates, disoriented and petrified, yelled, "fire!" 

Isa, part Dalmation, part Collie, returned to Maxwell House and tried to let herself back in. She has a new house, of course. And a bigger back yard. She has full-time access to the creek that runs behind her new house. She's got everything a dog could want, but she tries to revisit her old house at least once a week. I know how she feels. 

Before they let us in the house the night of the fire, all the flames out and gallons upon gallons of water dumped on our valuables, the fire marshal warned us there might not be much worth saving: 

"Now, you can go back in for a few minutes and stuff...but I'm telling you, there might not be much you want to save. I suggest you make a list and take it in with you." 

The six of us, the former residents of Maxwell House, now homeless and in shock, look at the marshal slack-jawed. 

I take out a half-sized index card, yellow. And begin to write.

-journals 
-camera 
-isobel 
-phone 
-passport 

Isobel, six years old this month, started traveling the world with me shortly after she was born. Our first trip together was to London. Unlike Isa, she seems happy with transitions and only really gets huffy when I cram her, last-minute, into an already over-stuffed backpack. 

#### A Town called Happiness & Success 

In Mexico at the end of 2006 I lived with an older couple for a few weeks. They really liked me but saw my lifestyle of transitions, new countries, new businesses, new languages...irresponsible. After one particularly painful argument I talked with a trusted friend and our chat went like this: 

me: He thinks I need to do the corporate thing so i can "retire" and I tell him I'm never "retiring." 
x: That's so old school of him. 
me: But he thinks i'm not doing enough for my "future" and I say now is my future. 
I mean, I am creating it in the present moment and living, arriving in each moment with my eyes open. That's a future that I'm working on every moment. 
He thinks I'm spouting Buddhist bollocks. I think i'm just telling him my worldview. 
X: Yikes. I hope you blog this. 
me: He called me a DRIFTER. 'Course I'm blogging it! He thinks I'm wasting my life. 
X: Fuck him. 
me: He thinks this is a "sidetrack" and that I have lots of "sidetracks." 
X: You're not riding on freight trains and fucking guys for smack. 
me: I said it depends on how you define "drifter." 
X: Sorry, G. 
me: No, no, it's good! It's all fodder. Get this. 

He said, "there is this town, called Happiness and Success and we are on a train, and most of us missed that stop and just ride the train on to death wondering how we missed the stop for that town back there." 

And I say to him: "What's the population of that town?" And he puts the question square in my lap. 

"I don't know, what do you think...10? 100? 1000 people?," 

I say, "I think that the train makes that stop in every moment. It keeps stopping there and we CHOOSE to see that in each moment (and that I don't equate happiness and success, or see them as linked) and it's not something that you miss along the way and suddenly you die 

I choose to "stop" in that town every moment of every day, and therefore see limitless possibility in my life. 

#### We're Always in Transition. Get on the Transition Train. 

Jeannie greets us at the Boulder Fire Department with a huge smile and hug. She's expecting us. We walk up the stairs to the office and notice the bulletin board with pictures of grimy firefighters involved in some heavy duty work. The training was to teach them how to cut holes in the roofs of burning buildings. Some of them grin up at the camera, showing off their chainsaws and sweat.”They did that to your house, too,” she tells us, gesturing at a picture of a precariously poised firefighter, angling the chainsaw while gripping his ladder. The temperature inside was so hot that they had to “pop the top” and cut a huge hole out of the roof, communicating with people on the inside through walkie talkies. “Must be terrifying,” I say to Sherry. She nods that it's one of the most dangerous parts of the job. 

I don't plan to talk about the fire “forever.” After my mom died I heard “time to get over it” more than a few times and I now believe that kind of sentiment is bullshit. Talking things out, sharing the story and gathering information about why/when/how (as my journalistic tendency to do so is anyway) is part of the healing. 

The six of us that lived together at Maxwell House have gone our separate ways for the most part. One of us went to India. One to New Mexico. Several of us have hung around the area but are ready to travel. I've got the itch. No surprise. 

Jeannie hands us the 27 page Fire Investigation Report as we pepper the staff and firefighters with questions. I ask where the brass pole is they slide down to go to fires. Everyone laughs except me I guess. 

Maxwell House Fire: The Stats 

Time of Alarm: 12:20 am 

Time of Arrival of First Fire Truck: 12:24 am 

Clear Time: 5:16 am 

Number of Fire Fighters: 25 

Event: Heat from a candle that was placed in plastic candelabra melted the candelabra which eventually ignited the wall covering and spread to other combustible materials. 

*** 

In Japan at New Year's kids play with a toy called the Daruma doll. For a while I lived in Gunma prefecture, where the dolls originate. Daruma dolls are famous for their rolly polly bodies that wobble, fall over and return to their upright position immediately. The saying goes, "seven times down, eight times up." Even when it's motionless, you get the sense it's moving just a little. Always wobbling, poised on the brink of falling over and prepared to bounce back to center. 

To be alive is to be in transition. Each time you inhale, your body converts toxins into nutrients. Whether you travel as far as the coffee shop down the street or down the stairs to the bathroom, you're constantly in transit. If, like Isa, you pine after the past, you may end up with glass in your paws but a sense of satisfaction at seeing the ashes one last time. If you look toward the future hoping for the train stop at Happiness and Success you may miss your life as it slides by you in the train window. The best way to honor the present? Pema Chödrön offers a refreshing perspective, "a warrior accepts that we can never know what will happen to us next. We can try to control the uncontrollable by looking for security & predictability, always hoping to be comfortable & safe. But the Truth is that we can never avoid uncertainty. This not-knowing is part of the adventure." 

I'll see you on the train. 

[Originally published by The Fempire](http://www.thefempire.com/read?entry_id=112&option=print)

***

#### It's 5:30a, Morning After the Fire

It was about quarter to twelve the night of the fire.

[Paul's](http://paulsalamone.com) in my room, talking me off the latest ledge. I've just been on a tirade against "conscious capitalism." Ironically, we have just gotten back from a class on nonviolent communication. To which I say, "fuck nonviolent communication, what's conscious about capitalism? Is its opposite zombie capitalism?" We're sprawled on the black futon I got when I came back from Japan. It was supposed to remind me of Japan, but instead gives me a back ache. It's quiet in the house. Paul's missed a spot shaving.

Mid-rant, I hear the sound of a large object being pulled across the floor. Upstairs. Scuffle sounds. Voices scrambled together. "Someone's breaking in?" I ask Paul. "Let's check," he says. His eyebrows bend towards each other, but his legs don't move. 

The sounds increase. Paul and I head towards the door. We meet at the back stairwell, all six of us. I can't make out what my roommates are saying. They're talking all at once. Finally I hear it: fire.

I'm shoeless and not wearing a jacket. But I dash outside to look up and see for myself. Confirmed. There's a fire. Flames reach out of the window. I call 911. They ask for the address. "We're already on the way," says the voice on the phone. It's just after midnight and I'm standing in the middle of a completely quiet street, very alone. This is disconcerting to me. Where are the other five?

As I stand there I notice the wind and, as improper as it feels to do on the quiet street, I yell. "Get out!" No answer. I yell again, "get out! Get the Macs and get out!" 

I run to the neighbor's house and begin pounding on the door. I'm breathless when Neils answers. "Our house is on fire," I say. I point up to the top floor. Neils adjusts his glasses. He squints, then says, "your house is on fire. Are you okay?" When I nod he says he's going to wake up his wife and son. 

About this point I hear sirens. The first ambulance has arrived and I see Paul standing with two of my housemates. Where are the other two? I wonder. I scuttle over. From this angle, the fire seems smaller. When I ask about the other two guys I'm told they're "trying to put it out." Fire fighters are now heading into the house. They've turned off their sirens. Another truck pulls up. The lights are flashing brightly, but no sound. 

Our neighbor from across the street, the one with an Alaskan husky, comes outside. She offers us her front porch. We walk over. I sit down, notice the flames have increased in size, and call [my grandmother](http://nunubell.com).

"Gwen, honey, what's wrong?" she says as she answers.

"The house," and I can't say anything else. I'm sobbing into the pay-as-you-go phone. The one I'd drop into a dive toilet. For now, I hand it over to Paul.

"Hi Nunu. It's Paul. Our house is on fire." 

He hands the phone back to me. I am breathing deeply and recalling every yoga pose I've ever done. This is it! This is the time those classes are supposed to pay off! I think. And, I calm down a little bit.

After talking with Nunu, letting her know I'm not dead and I'll call her in the morning - the actual morning - she's on the East Coast and I probably should have thought through that before calling. I wished she was there right then. Sitting on the porch with me. Telling me what to do next.

Turns out, she didn't need to tell me what to do next. While we were talking a third firetruck had arrived, and shortly thereafter, the Red Cross. I look at one of my housemates and say, "Oh my God. We're refugees." 

The Red Cross was like a grandmother. Like one hundred grandmothers rolled into two people. They got there and moved us off the balcony and into the house of my neighbor with the husky, these two volunteers. One, a balding man in his mid-forties, perhaps. He looked astonishingly alert for four in the morning. The other, a woman probably in her late thirties. They seemed to glow faintly. I remember them possessing beatific half-smiles, nodding at us noncommittally as we asked questions: will we be able to go back in there and get things? Where will we live now? 

The fire marshall had already told us what to expect. Not much.

"Do any of you have renter's insurance?" he asked. "What's renter's insurance?" and "no" came the confused reply.

"Alright," he said, "now, we're letting you go back in there. One at a time. And you have to put on some boots or something, you can't go in there barefoot. You can go back in for a few minutes and stuff...but I'm telling you, there might not be a whole lot you want to save. i suggest you make a list and take it in with you." 

I take out a yellow half-sized index card, and write a list. It's hard to remember what's where, but I imagine the path I need to take to find my beloved objects.

We were to make a list. A short list of what we'd get if we were allowed back into the house. Which we may not be. We may not ever get to go back in the house. 

I started on my list. The fire marshall marched off in the direction of the fire.

We filled out the paperwork, including our individual reports on what we'd heard, seen and done during the early moments of the fire. When that was done, the fire marshall came back. He seemed dirtier, but still glowy-looking in all yellow.

When it's my turn to go into the house, it's hard to make out what's happening. It seems like there was a thunderstorm in the living room. The floor is soaking wet, there's no power and the sound of heavy boots on the ceiling echoes the sounds from a few hours earlier. Someone loaned me a pair of boots to wear in, so I'm clomping along looking for the objects on my list. A fire fighter lights the way with a flashlight. 

On the way to my room we pass floating objects. It's dark and then suddenly light, as he shines the flashlight. Dark again. Then light. Imagine a silent picture film, then add a downpour. Shake on some soot.

I find my journals, damp but intact. The camera is wet and smells like a fire. Like a campfire, though. Not a house fire. I spot Isobel, a koala I've had through years of travel. Grab her by the mangy paw. Stuffing all my must-gets, including my passport, in the crook of my arm.	

Daylight approaches and the blonde Red Cross volunteer gives each of us a map to the motel they've secured for us, a pre-loaded debit card and a provision kit. "You may use the cards however you wish, but you can't use them to buy alcohol or weapons." The six of us agree to do breakfast in the morning. I glance over at Paul. The rest of his beard has grown in, making it hard to see the unshaven patch. I say, "we have client meetings we need to cancel." 

Then the motley gang piles into cars to head to our sponsored shelter for sleep. It's five thirty in the morning, the morning after the fire. 

