---
title: Ls Not Click: Fumbling onto the Command Line
date: 2014-01-15
---

I'm in wide legged seated forward fold. The machine on the floor, my toes pointed towards the ceiling.

A current client sent a message saying he's practicing remembering to 'ls not click'. I had to read that three times before I understood what he meant.

	ls

is a UNIX command that tells your machine to list files. ls. Not click. 

He's working on getting over GUI dependence. GUI. Graphical user interface. Why? I can't speak for him, but I will speak for myself. On my way off mind-manipulating, and therefore life-manipulating GUIs, I had to fall. Hard. Two things happened during the fall. 

1. I played Twilight Imperium. 
1. During my "recovery" from losing TIIII - bad - I read Alan Watts' _The Joyous Cosmology: Adventures in the Chemistry of Consciousness_. He writes about his LSD trips.

Something about being broke, almost homeless and broken open by too many game nights (side note Castles of Burgundy was an amazing one, and isn't a sixteen hour time investment) put me on the wavelength with a Debian guy who couldn't believe I was still on a Mac, when I value privacy and freedom and honesty. 

He started me thinking about 

	ls

not click. He planted the seeds for me to learn [Git](http://git.gwenbell.com).

He also lived in his van. May have been a cautionary tale. 

***

What happens when you do it all yourself even just once? 

If you learn to 

	ls

not click, you experience something important. You'll experience yourself commanding the machine to do what you want. You'll see you're the boss of the machine, not the other way around.

If you're searching for freedom and privacy in your computing interactions, you might try [Arch](/arch). For me, learning to 

	ls

not click has been the single-most freeing thing I've done since I started using the computer lab in grade school. 

Learning to 

	ls

not click is a return to grade school. In the best way possible. 

It's awkward. I didn't know where files were that I wanted to get at, nor how to make a new folder when I wanted one. Without the mouse, I didn't know how to get around my own digital house. Getting onto the command line each day for a year and a half... I got it. I bet you can, too. If you're willing to empty your cup.

To always being beginners. 

(Just make sure the [door's locked](/america), hear?)

_Gwen_

By the way! I still use a GUI sometimes. GIMP for photo editing. And if I'm going through my hard drive, I'll use Enlightenment sometimes, for speed. Rest of the time, [xmonad](approach).

