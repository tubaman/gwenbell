---
title: An Engineer Like You
date: 2013-05-09
---

I called Bub to remind him of when I was little and I'd beg to go with him to the hospital.

Bub worked shift work his whole life, and most of the time I knew him growing up, he did the shift work in a hospital.

We're both engineers, though he did his work with his hands. Bub never used a computer for work, but he did have a man cave he'd go into when he got to the hospital to start his shift, and that was one of the things I liked best about it. That there was this secret place in this ginormous hospital where he, and sometimes I, could go hide out.

The engineering break room was long and narrow with a bed in the back against the wall. There was a pinup calendar with voluptuous women in tiny outfits in front of motorcycles. The room smelled of tools, of grease and of men, since those were the only things in there most of the time.

I actually hated hospitals is the thing. I didn't want to be in the hospital itself, because it was the same place my mom spent a lot of her five years dying.

Anyway, engineering. Bub's shift. The thing is. When I called him to remind him of how I loved going into work with him, he didn't start reflecting back what I was expecting to hear. He said

Honey, you remember when you wanted to take a shower out there after the hurricane?

No, what do you mean take a shower? At the hospital?

Mmhmm, right. I took you in there in the Renault and you remember that curve at the KOA, a tree came right in through the windshield. Whole tree branch.

OhmygodBub. I could have died!

Sure right you could have. Nearly got your face, I... (he trails) You don't remember that?

Well, huh. Yeah, no. I guess I don't.

Well, we turned back and got a different car. And then I got that generator so you could take showers. Yup, sure did.

<hr />

Sometimes I don't get out of him what I went into the call for, but I get something that jump starts another line of thinking. Which of course is ok with me. Bub's in his seventies now, which you'd think would dull him. But the opposite has happened. He remembers a whole set of data that I have altogether forgotten. He has an engineering mind, he sees systems I don't.

Looking at the hospital as an engineer isn't the same as being a patient there. I didn't feel scared when I hung with Bub. Even when very little, when
I felt lonely or sad or tired I'd grab a hand, and his hands were usually a little greasy, and always warm. And I knew I could trust him as we navigated the labyrinthine hospital up to the roof. Where we'd check in on the fans. And then down to the generator room, which was still running during the hurricane, hence the shower I was able to take.

The generators can't go down at a hospital. That's the mission critical room. Generators make it so any terrible thing can happen and those patients on respirators, those patients who are in compromised situations, they still get the support they need.

<hr />

As I've become an engineer myself, I experience the joy and challenge of being relied upon. Not relied upon as in if this goes down, people will die. I'm not that kind of engineer. But when I see an ECONNREFUSED I know there are people on the other side of their machines getting something other than they expect. I go into the generator room that is my virtual private server, and I start -ef'ing around, killing and restarting processes.

And, I go into diagnosis mode. If I can't right the ship immediately, I step away from the machine and reapproach again, once I've thought about it.

Becoming an engineer has slowed me down. I think in a different way now.

I like to imagine I think more like Bub.

<hr />

Before we hung up the call I said

Hey Bub, listen. I don't think I've ever really told you this, but I respect the work you've done. Thank you for showing me how I could become an engineer like you.

Mmhmm, well, alright honey. Call your [grandmother](http://nunubell.com), she's over at Et's.

Gwen <br />
9 May 2013 <br />
Brooklyn, New York <br />


