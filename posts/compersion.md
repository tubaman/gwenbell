---
title: Compersion
date: 2013-06-01
---

Last night I sat in front of a fire at a rooftop dance party in Bushwick, New York. I was drinking a Weihenstephan, the world's oldest beer. I let the bass from the dj's speakers vibrate me. 

As I listened and drank and watched the fire, I interviewed [Justin Orvis Steimer](/thirdleg), the artist with the claw-footed bath tub in his bedroom. 

When he walks into a room, clothes start dropping. I wanted to know his secret, so I asked for an interview. As we talked, his arm draped around my shoulder, the smell of his sweat mixing with mine. A moment.

I have those moments all the time. 

I derive pleasure from other people's pleasure. 

In the poly world, there's a word for it. [Compersion](https://en.wikipedia.org/wiki/Compersion).

Though I'm not currently in, nor seeking a poly relationship, I've been 1/3 of a poly relationship. I was intrigued as I read about compersion, because I feel it so often and in all my reading and travels, I'd never heard of it. 

Compersion may be _the_ secret the mainstream monogamous community doesn't want you to know about. As though the mono culture will break when you derive pleasure from someone else's pleasure. Instead, it focuses on drama and jealousy. Two indicators there's a paucity of pleasure to be had, and you gotta fight for what's yours. I say there's _more than enough_.

So, let's focus on that, the other side of the coin.

#### Compersion

One morning in [Sayulita, Mexico](/expat) I watched a woman add cream to her coffee, and her hair was long and bleached blonde, and her dining partner had his arm around her shoulders and a little girl ran around at her feet. She had about twenty-three bracelets on her left wrist. Sunglasses. There is no way I could ever pull off that look, nor would I necessarily want to. 

Instead, I enjoyed her enjoying herself.

As I watched her take that first sip of her coffee I felt it. I mean, of course, I can't feel what she feels, but I felt compersion. Just seeing her delight, her love directed at that child, her excitement with her partner, and the delicious coffee to boot.

I wish we talked about compersion more. Rappers rap a lot about jealousy, and recently I've talked about it, too. But I don't do jealousy. I rarely experience jealousy. I do compersion. I feel _that_ daily. My turn ons are legion. If it doesn't go without saying, I'll say it. These links are clean - no affiliates. I wouldn't be telling you if it didn't turn me on for real.

#### The Ethical Slut

There's a section or two on compersion in [The Ethical Slut](https://en.wikipedia.org/wiki/The_Ethical_Slut). If you want some new sources of pleasure, or at least tittilating conversation, buy a copy and then read that book in public.

After the conversation with Justin on how he turns the room on, I had a conversation with a fellow developer. I asked him questions, too. He had a harder time getting at what brings him pleasure. He wasn't even sure which programming languages he enjoyes programming in. 

So, I'm bringing two worlds together here. The analytical world, those of us on the command line, need to remember to access to the world of [pleasure](/turnon). 

Start a pleasure log if you want. Or just take a little pleasure from the pleasure I take. 

Practice compersion.

<div style="font-size: .8em">Updated 2014-02-6</div>

