---
title: 5 Questions for Marco Larrazabal: You Made Me Delete My FaceCrack Account and it Felt Good
date: 2013-12-16
---

In today's [letter](/handcraftedlist) I told you I'm doing an interview experiment. Marco, you're up!

#### Marco Larrazabal Didn't Pay Attention Until I Became a Programmer

**GB:** What are you working on right now and how's that going for you?

**ML:**  Right now I'm bullsh*****g my way through my thesis, but on the
side starting on HTML/CSS (using notepad++),.and some game development
(with a GUI, so I can test my ideas faster until I learn C). No
website yet.

**GB:** How'd we cross paths?

**ML:** I stumbled on your work after Ev Bogue nuked Far Beyond The Stars
and was publishing exclusively on Poogle Glus, but I didn't pay much
attention until you (and Ev) became programmers.

**GB:** What about the work I'm doing right now resonates for you? What about it has you fired up? What about it has you totally lost? What about it infuriates you? How can I improve your experience of the work I do?

**ML:** Well, you made me delete my FaceCrack account and it felt good.
Nothing about your work infuriates me. I'm fired up on Git, I'm lost a
bit on the code snippets you sometimes put in your writing (but soon I
won't be that lost).

**GB:** What does 2014 look like for you? What major changes/breakthroughs do you see yourself making in 2014?

**ML:** WHAT'S IN YOUR STACK? 10.1'' Netbook dual-booting Windows 7 (don't
kill me) and Fedora 19. Using Notepad++ on Windows for HTML/CSS stuff.

**GB:** What was your biggest lesson learned from a mistake in 2013?

**ML:** GTFO of Multilevel Marketing!!!

#### Bonus Question from Marco

WHAT'S IN YOUR STACK? 10.1'' Netbook dual-booting Windows 7 (don't
kill me) and Fedora 19. Using Notepad++ on Windows for HTML/CSS stuff.

#### Followup Email

**GB:** Interview up! PS: Why don't you have a website?

**ML:** I had a Wordpress site until Skynet (that botnet) took
it down. Right now I'm learning HTML/CSS making my website. It will be
online soon and for the moment will be using MODX until I learn how to
host a site using Git (not on Github).

