---
title: Mistakes
date: 2014-01-14
tags: learning, practice
---

<img src="/images/gamansters.jpg" class="profile full">

> A life spent making mistakes is not only more honorable, but more useful than a life spent doing nothing. — George Bernard Shaw

LeoTal, a mod in [#lesswrong](/irc), asked about my biggest lesson learnt from a mistake when I popped into [what's more or less gwern's channel](http://gwern.net). This page is my answer to that (loaded) question.

#### The Trouble with Mistakes Were Made

"Mistakes were made." 

I hate that phrase so much. I think it adds insult to injury. 

Not "I fucked up" but the intentional passive tense..."mistakes were made". I heard that so much in 2013, but [most and worst and shittiest of all from the tech world](https://news.ycombinator.com/item?id=6873703). 

"Mistakes were made" to let themselves off the hook, rather than saying "this company, while I was at the helm" did x.

That frustration and LeoTal's question birthed this mistakes page. 

Not to focus on mistakes. But to acknowledge I have fucked up. And to show how I have -- or am -- course correcting. Not just a log for myself, but a log for the world. It is not always rainbows in butterflies, and this page proves it -- to you, to me, to errrybody.

#### Singapore Shrimp

<mark>Lesson Learned: Stay Out of Singapore</mark>

I will never return to Singapore. [William Gibson](http://www.wired.com/wired/archive/1.04/gibson_pr.html). It's Disneyland with the death penalty. 

In the second month of my stay I ate a Vietnamese Spring roll. Fresh cilantro, shrimp that had been cooked and then refrigerated perhaps, lettuce in a rice wrap. It is the sickest I've ever gotten off a dish. It was diarrhea-vomit; bringing me to another mistake. I should have puked right away but didn't.

I was down for a week. I thought I was going to die in Singapore. I did not die. I _wanted_ to die. I held very still and made peace with my impending death. 

I told nobody besides [EB](http://evbogue.com). I figured I'd be quarantined and never heard from again.

It's something I learned from the cruise I took one time. The all expenses paid, and it was the most well known cruise line in the world, free trip. After that I swore I would never take a cruise again. The old guard told me not to make a peep when you're sick; they'll quarantine your ass. Take note cruise ship wanna-goers. For more, read DFW's piece [A Supposedly Fun Thing...](http://harpers.org/wp-content/uploads/2008/09/HarpersMagazine-1996-01-0007859.pdf).

Back in Singapore, I am finally puking and I swear I will never go back to Singapore, and that I'll never eat shrimp again. E coli. Please never again.

#### Mawwaige || See also: [Divorce](/divorce)

<mark>Lesson Learned: Stay with Gut; Don't Sign Long-term Contracts</mark>

I've written [a thing](http://gwenbell.com/divorce) about divorce. 

I should have stuck to my guns on the marriage front. [In 2007 - thank you Wayback - I wrote about how I wanted a micro-marriage](http://web.archive.org/web/20071012221738/http://www.gwenbell.com/blog/2007/10/07/micro-marriage-a-new-kind-of-proposal/). A marriage that both parties agreed to for a set period of time.* 

In the end, though, I wanted to be part of a family I thought was pretty cool. I loved the guy. We went for it. 

Long side note: the need I was attempting to meet, in NVCpeak is 'belonging'. Now that I know what I'm needing at all times, I don't have to make so many long-term commitments. In fact, I can make zero. Speaking of, check my [appreciations](/appreciations) page for ANVC, which is, to my mind, a more honest approach to NVC.

**I am not wife material.** I even got a hedgehog to try to settle down. But I was terrible at it. I felt trapped, I gave myself Bells Palsy for a couple of weeks. I didn't buy the family thing the way I needed to to do it well. 

That, and I prefer other things to settling down. Learning, [traveling](/expat), pushing myself, pushing my readers and having minor breakthroughs on occasion. 

If you've read Steven Pressfield's _The War of Art_ you will understand when I say marriage was, for me, The Ultimate Resistance. 

I course corrected by initiating divorce and ending it within two years. 

After [it went pear shaped](/divorce).

#### Geoarbitrage Only Works When You're Hustlin' in the Right Direction

<mark>Lesson Learned: If You're Going Broke, Go Somewhere Cheaper</mark>

In 2013 in Bed-Stuy I saw a guy wearing a hoodie that said, "You're huslin' in the wrong direction," and of course because I was going the opposite direction he was, he must have been hustlin' in the _right_ direction.

Geoarbitrage: earn more than you spend. 

Only works when you hustle in the right direction. Going broke? Don't bounce from Singapore to Japan to Berlin the way I did in 2012. 

Don't go to Singapore in the first place. But where instead? If you earn in USD, go somewhere the dollar is stronger than the local currency. That's Geoarbitrage 101. If you fail Geoarbitrage 101 as a location-free person, you're hustlin' in the wrong direction.

It's fine. You wind up in a basement where you learn harder tech skills. 2013: [Git](http://git.gwenbell.com), Node, Bitcoin, cryptography, whatever. In my case, I also watched every episode of Six Feet Under. 

#### Thinking I'd be Dead by 30

<mark>Lesson Learned: Beliefs Shape Reality</mark>

I thought I'd die by 30 and spent a lot of hours in panic about my health until I hit 30. I was totally sure I'd be dead at the same age [mom died](/momday), 30. Because if your mom dies at 30, you must have to die at 30, too. 

I was wrong.

#### Not Knowing About Quantitative Easing

<mark>Lesson Learned: The US Government's Printing Money</mark>

I wish I'd known about QE in 2009 when I got married. Because they'd just started that bullshit back then. And perhaps had I known I would have said, "hmm...there's a reason people are so flush with cash right now. The gov's printing money!!!" 

But I didn't know about it. And now that I do, I see things could be a lot worse than they are.

The other thing about not dying at 30 is this: I did everything on my "bucket-do-before-you-die list" and now I'm ancient. I mean, I did everything I wanted to do, and that made me at least 1,000 years old. Then I hit thirty and I (I just shrugged) well, it's all pruning from here til death, I reckon. 

Nothing else major on the life list. Which let's me off the hook to really get into the heart pulsing moment.

Which. Yes.

Oh! Speaking of hearts that beat! I eat them now.

#### The Vegetarian Thing

<mark>Lesson Learned: Meat Stabilizes</mark>

I wouldn't have been so veggie so long. 

I was pretty much hungry all of my twenties. Emotional crashes each day. In the decade plus I was vegetarian I never held it against meat eaters. I just wanted to do the do no harm to animals thing. Though, I'm an animal, too. And I was doing harm to myself.

That said, I have no plans to go the [raw meat mono diet](http://www.vice.com/en_uk/read/this-guy-has-eaten-nothing-but-raw-meat-for-five-years). [Why?](http://www.theguardian.com/lifeandstyle/2013/oct/02/raw-meat-mono-diet-nutritionists-comment)

Good news about eating meat: breast size doubled, or maybe even tripled. 

Booyah!

#### Pedagogue of Young Gods (excerpted from Niggy Tardust)

We all hustle and grind, <br />
any system against us is against the divine. <br />
But there's no sense of glory in repenting, <br />
and repeating, <br />
their mistakes. <br />
You have a greater calling. <br />
Answering it is all it takes. <br />
Take a second to hear this <br />
and go back about your day.<br />
Know that laws don't govern us, <br />
we're governed by what we say. <br />
What we think, why we think it, how we handle. <br />
Place no blame, point no fingers, take your aim. <br />

Shoot to kill. The bullshit."<br />

**Saul Williams** _Niggy Tardust_<br />
