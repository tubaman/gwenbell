---
title: 10 Tech Tools You'd be Ill Advised to Learn
date: 2014-01-30
---

My site is set up in such a way that it's much more secure than other people's. It's just second nature to me now. I don't have to work at it. So now I can spend all day
painting instead.

<img src="/images/screening.png" class="profile" style="width: 100%" />

#### 1. Git

It started with Git. Every commit is cryptographically hashed to the previous hash. And you'll not only know what that means, you'll be able to do it.

Remember when you had those candy necklaces as a kid and you ate one off and the necklace stayed together? That is not quite the right metaphor, and to tell you the truth, a better metaphor is one I used when I taught Git at the Flatiron School in Manhattan in June. And that is, like Voldemort, your work is more secure when it's distributed.

The thing about Git is this. You push your work to remotes, and not just one remote.  Why? Because you know how SPOILER ALERT Voldemort divides his soul and puts it into receptacles and deposits them in more and more obscure places, and you can only destroy him if you reunite all his soul and demolish that? 

You do

	git remote add [name of the remote] [location of the remote]

You're adding your work to another place, making it more resilient.

Also. I will tell you this, because of Git, I am a better writer, and a better coder, and possibly a better lover (you will have to ask my many lovers to confirm).

And. AND!

If you call yourself a programmer and don't know Git, you don't know shit. I do [one on ones](/pairprogram) if you need to get this shit fast.

9/10. Hard to learn, but not impossible. The reward is worth more than the effort you'll have to put in learning it. Once you learn it you can contribute to the work of other programmers, and they to yours.

#### 2. Vim

Top benefit of learning Vim: you don't leave the command line when you need to make an edit to your work.

Another tool that will not make you a better lover, but will make you nerdier. 

Vim I avoided because I thought Sublime Text 2 was just as good. And I'd already upgraded from Scrivener to Sublime Text. 

Then Sublime Text 3 didn't come out and didn't come out, and it cost money anyway, and Vim is free. Also, the guy who works on Vim, he just struck me as a funny guy I'd like to spend time with.

And as with Git, the aforementioned hard technology, it is a three letter word created by someone with way more years on the command line than I (will ever?) have. You have to trust (and then verify) these guys know more than you about programming.

Two tools that helped me learn:

1. vim-adventures.com
2. vimtutor is built in

7/10. Learning Vim forces you to think about things in a different way than you do now, if you use Sublime Text or another text editor.

#### 3. The Terminal aka The Command Line aka The CLI

Top benefit of learning the command line: gateway drug to programming.

Had I not gotten onto the command line, I would not be here telling you any of this. I'd be using Sublime Text to write poetry or something.

Getting the basic commands down - learning how to 

	cd 

around and

	rm -rf
 
to get past gui-dependence, to use the machine how it's meant to be used, as files and directories instead of cutesy smiley faces or whatever chipper icons [A**le](/revolution) now uses for what it calls an operating system.

8/10 on the Gaman Scale. Not that hard to get down the basics, but a lifetime of learning codes to make your life even better.

#### 4. Arch Linux

Top benefit of learning Arch: freedom in an operating system! Can't overstate this one.

I've written about the [benefits of Arch](/arch), and I'll recap for you: it's hard, you might brick your machine. And it's the best operating system I've ever used.

Before this I used Mac OSX, then Ubuntu, then Arch - first with Gnome, then with Enlightenment, now with dwm. 

All told, the transition took about a year, including six months on Ubuntu, switching back to Mac for a few months, and then onto Arch. I can't see myself using anything else.

10/10 Hard. Life-changer.

#### 4a. xmonad

[xmonad](https://en.wikipedia.org/wiki/Xmonad). It rewired my brain.

xmonad changed everything for me.

#### 5. Haskell

You're looking at a Hakyll site (built with Haskell). [Clone it down here](http://gitlab.com/gwenbell/gwenbell).

9/10 Hard. Time-consuming, and a life changing way to invest your time. 

#### 6. Living Phone-Free

Top benefit of living phone-free: no distractions.

10/10 Hard habit to break. I believe it's the best possible digital addiction to quit. 

#### 7. IRC

Top benefit of learning to use [IRC](/irc): people are frank in IRC. It's okay to admit you don't know, and somebody, in some channel somewhere, will help you.

While you're at it, use [tmux](/tmux) to log the channel. (I formerly suggested Screen, but it's been deemed harmful by the tech community.) You can find me in #gaman.

7/10 Not too hard to learn, but kind of a pain in the ass to blend into your workflow if you're used to Xting and gchatting.

#### 8. VPS

Top benefit of learning to use a VPS: peace of mind.

A VPS (virtual private server) is a tiny virtual machine that runs all of the time on a super-fast internet connection. It's virtual, because there are many tiny virtual machines inside of a big rack of superfast computers... somewhere, depending on where you get your VPS from.

Have you ever had access to a timeshare? This is a timeshare on someone's supercomputer.

That way, you don't need a hosting company that tries to do all things - it's the UNIX philosophy - one thing well.

With my VPS I have an always-on Arch Linux, which is how I run [tmux](/tmux).

If you use [Digital Ocean](https://www.digitalocean.com/?refcode=14a7846491d4), you throw me some referral credit.

6/10 on the Gaman Scale. If you already know Git, FTP is a moot point, and VPS is an obvious next step.

<del>#### 9. CJDNS

Top benefit of learning to use CJDNS: security through C.

[CJDNS](https://github.com/cjdelisle/cjdns) is a secure routing protocol that rides on top of IPV6 created by CJD. If you want to learn more about it, visit Derp and Luke's [Hyperboria explainer page](http://hyperboria.net).

8/10 You can do it, but it will take time and patience. On the other side of that patience is a smart group of people.
</del>

#### 10. SSH

Top benefit of learning SSH: a cryptographic connection over insecure networks.

You can use SSH to control another machine over an Internet connection without anyone seeing what you're doing. You generate a private key on your machine, and hand a public key to your VPS and voila! Security.

You've seen the word cryptography over and over again in this piece. You're going to see it here one last time before I let you fall asleep and chew on this. Because, when you're [doing hard things](/gaman) you gotta rest and let it seep in.

7/10 SSH is a network protocol, in a simliar vein to email - you don't have to understand how it works to use it. 

<div style="font-size: .6em">Updated 30 January 2014</div>
