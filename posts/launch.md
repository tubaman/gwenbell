---
title: How the Launch Sausage Gets Made
date: 2014-01-02
---

#### Why Launch

I launch because it's the best way I know to get a complete product into the world. I ship new offerings every two to three months. 

I've been launching since 2010, with my first solo offering, Digital Warriorship. I launched it out of tiny cafes in a mountain town. I launched it with Ebookling, a site, now defunct, I thought was cute. I was sad when it failed.

I launch for two reasons. More than two, but let's start there.

1. It puts my feet to the fire. Knowing I have a launch coming up, and that I have to finalize a product by a pre-determined deadline focuses me
1. I make my living launching

Before I launch, I [book](/book).

#### The Most Important Thing

I have a launch partner. I have a dedicated someone say, "GB's in the middle of a launch,". Why? Because everyone thinks they are the most important person in the world. If you don't have someone protect the launch boundary, they -- all sorts of people -- swoop in and vampire suck on the vital energy you've worked up with your morning launch prep session. 

People can just _feel_ you're in the middle of a launch. 

And they want _that_.

#### Other Things

1. Hire or beg someone to be your launch partner
1. Have a dedicated launch pad. It can be tiny
1. If possible, disconnect the internet leading up to the launch, then be sure you have a steady connection during
1. Keep the scope tight; you're launching one thing, not a dozen
1. Eat! Best case? Be fed. I had a live-in chef for the _Write Before You Die_ launch and it meant everything to its success
1. Set one modest goal and one outrageous one
1. Be sure [your website is aligned](http://align.gwenbell.com) then don't touch it during launch besides the offering page
1. Offering pages take years to master, so stay with it and get something out the door, even if it's not perfect
1. And, finally, lower your expectations; release all thought of fame, glory or riches. Detach from results

#### You Learn to Love Launches

I've loved every launch I've ever done. I get to the end of each one spent, empty cup.

I've launched from Europe, Asia, Latin America. Even though some launches do better than others on the financial front, they all challenge me and grew those who buy and engage with the work. I still have people email me to thank me for that first launch, way back in 2010. 

I now have a sizeable body of work because I launch.

For those of you on the receiving end of this latest launch, thank you for your presence. It's not a launch without launch party participants. Some of you have been with me since that first launch in 2010; I would not be where I am now if it weren't for you. May my work continue to support yours as we dive into 2014.

#### Extra Credit: IRC

        00:12 < mil3s> gwenbell, what is a launchpad, so i can find a tiny one :)
        02:59 <@gwenbell> mil3s: hey!
        02:59 <@gwenbell> a launch pad is a, how do I describe it
        02:59 <@gwenbell> It has just what you need, nothing you don't.
        02:59 <@gwenbell> A bed, two pillows, blankets, a mattress. Two towels, some kitchenware, two chairs, one table, a bathroom and a small closet.
        03:00 <@gwenbell> Also, a way to prepare food, but not a whole ton of food, just what you need.
        03:00 <@gwenbell> Think a hotel room, but bigger and with kitchen utensils. But not as big as a full on house.
        03:00 <@gwenbell> Thank you for asking!
        03:05 < mil3s> so a launchpad for a launch refers to simple website, simple way to pay, simple way to distribute?
        03:09 <@gwenbell> oh, no
        03:09 <@gwenbell> simple way to live
        03:09 <@gwenbell> so that you can make it to launch
        03:09 <@gwenbell> so that you can make it to launch
        03:14 < mil3s> i gotcha, throw off all the dead weight, like when i go on my survival hikes, small back pack knife, fishing kit and thats it, no fe
        03:14 < mil3s> nonfood except what i catch
        03:17 < mil3s> i want to do a launch, what do i have to do?
        03:19 <@gwenbell> 1. get a launch partner
        03:19 <@gwenbell> 2. be clear on what you want to launch
        03:20 <@gwenbell> 3. check with people already in your circle that it's something that'll meet one or more of their needs
        03:20 <@gwenbell> 4. triangulate; you're already doing that here and in #venportman and around the web (where else besides irc?)
        03:20 <@gwenbell> 5. get your skill set to a stronger place than those who follow your work
        03:20 <@gwenbell> 6. take feedback, but not to heart
        03:20 <@gwenbell> 7. launch, repeat

Which reminds me. New [appreciations](/appreciations).

<div style="font-size: .8em">Updated 27 January 2014</div>

