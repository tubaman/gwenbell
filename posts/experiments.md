---
title: The Facebook and Google Experiments: July 2012-October 2012
date: 2012-07-01
---

I updated this piece on 22 January 2014. I fixed typos and deleted non-relevant bits. Otherwise, it survives intact.

I wrote these notes from July 2012 when I lived in Japan (with a person who was getting ready to go intern at Facebook, and persuaded me to give it another try). During that time I re-wrote and published Digital Warriorship for the second time. At the intern-to-be's persuading, I decided to go into Facebook to see if it was "that bad." 

It was a disaster for me, as these notes reveal. 

Following that you'll get my notes taken during my time in KCMO for the Google Fiber rollout. If it weren't for Isaac Wilder (of the Free Network Foundation) I would have called that trip a total bust, too. Facebook and Google are now on my permanent [no-fly list](/tech). I went into the belly of the beast, twice, and upon emerging decided I'd had enough. And if it meant I had zero friends and zero new relationships in the future, I was willing to make the trade-off.

I got a message from the faeries during that time that said: both companies are dens of thieves. Enter at your own risk. 

For me, the price got too high, the rewards non-detectable.

***

#### July 2012, Japan, Facebook Experiment

Yesterday and last night was completely crazed. (FB experiment day 2). (2014 note: this was meant to be a 168 hour experiment to find out how it was in there. I guess someone flagged my new profile because I'd been off FB for two years. That got me a note from FB HQ. They wouldn't let me back onto the site until I provided government ID _proving_ my identity. That was, and still is, a red flag. I didn't make it 168 hours in FB, I left after less than 48 -- deleted my account a second time, and never turned back.)

Started researching dopamine this morning. That's exactly what it was. The overload/overstimulation led to heart rate up, pulse up. Up up.

But as Emily Yoffee points out in her article - it's up, without resolution.

#### Observations

+ Facebook. Sex, without climax.
+ I can't be a coach or marketer. Everyfuckingbody on FB claims to be a marketer. Or coach. Or marketer slash coach.
+ Asking hard questions on FB is like asking hard questions in Singapore. You just don't. Not part of soceity. Results in "defriending".
+ People tell themselves whatever they want about themselves. FB is a platform on which your one-arm handstands of 5 years ago are still "truth" - as in, if you don't notice my chubby thighs I won't notice your public drinking habits.
+ Honest reflection. Not what people want. They don't want to buy it. They don't want to hear it.
+ Where does this leave me?
+ In the past few days all kinds of ppl have come out of the woodwork. All of them with unasked requests. What is the request?
+ Is there any going in without going in? 
+ Like when Fleet does prison work, he doesn't get locked up again. He visits the streets. He doesn't live on them.
+ Hungry ghosting
+ Learning to feel full
+ Seeking/treadmill
+ Kairos Time
+ Awake to your world

Then you find a place
in the world
where everything just
makes a whole lot of sense

#### Open Questions July 2012

**Financial**

+ Will Bitcoin change everything?
+ What happens in a world w/o money? When everything is free and we're all at 0?
+ Is the rush to 0 spending right now actually a good thing.
+ How little are you spending? Are you budgeting/rushing to 0 yourself?
+ Is it no longer true you have to spend $ to make money?
+ Why am I just learning about Financial Easing? Why aren't people outraged about it and who among you can explain it to me in lamens?
+ How you earn - right now. There are three professions that most of the 3,000 people (2014 note: in 2012, on Google+, before I deleted my account) who've circled me claim to fall under.
	1. photog\
	2. design\
	3. coaching\
+ If you are one of these professions - are you actually landing paying clients right now? If not, how are you paying your bills?

**Modern Life**

+ Why is the word "friend" and "friending" so problematic?
+ Are we culturally sick of sharing?
+ Is the word _share_ totally flattened/hollow at this point?
+ Is saying "I really love connecting in person" and then not doing so - the malaise of our time?
+ What are we each 
  thinking 
  but not 
  saying.

#### 23 July 2012

Seattle, Mex, SF, Singapore, Japan, Berlin (2014 note: that's the route I took from end of 2011 through 2012)

July 2012: big cameras, iPhones last stand, glass not out
July 2011: email. fb, twitter, blogs/sites

bit coin -the creative scene (2014 note: no idea what I meant by 'the creative scene')

TRUST

lack of trust on the web

unmet needs (I'm observing):

+ meaning
+ purpose
+ self-respect
+ self-presentation

#### 7 Aug 2012

<mark>Transition from Berlin to America</mark>

+ Child abuse ad coming into O'Hare
+ No seating; is this b/c people have been camping out at airports?
+ Starbucks - guy tried to game system & get free coffee
+ shooting while we were there
+ speculation, hedging & faux-thusiasm

#### KCMO: Google Fiber Launch, August-October 2012

<mark>After Chicago, I went to Kansas City, Missouri, to watch the rollout of Google Fiber. It was eye-opening and made me sad. I wrote these notes during the rollout.</mark>

Things I've observed

+ Hedging between people (reminds me of saving face in Japan)
+ Unexpressed boredom indicators
+ Cars barely running
+ Nobody seems to be walking except me
+ Abandoned bldgs, cars
+ Suspicion/paranoia/video monitoring
+ Housing industry is \'85 ???
+ $ in limbo
+ Food sources: Chipotle, Starbucks, and like 3 other restaurants
+ Booze on lockdown at Wallgreens

After the Google Fiber rollout I [ran out of money](/broke). 

I moved in with my grandparents for three months, learned [Git](http://git.gwenbell.com) by committing every day for 31 days, then taught myself UNIX/Linux and started in on [Arch](/arch).
