---
title: Build Your Perfect Machine
date: 2014-01-27
---

Arch Linux gets a 10/10 for freedom in an operating system. I've used it daily since September 2013 and have helped three people install it on their boxes.

#### Why? Because Freedom. 

This is the lightest, and therefore most freeing (light because free - engage your abs right now, it'll free up your shoulders and upper body) system I've ever used. When I discovered it has been around for years, I was shocked. 

It doesn't have a marketing department, and few people who use it talk about using it, so finding it is sort of happening upon a secret kept by thousands of people.

#### How?

If you want to get Arch on your machine, read through the free [Beginner's Guide to Arch](https://wiki.archlinux.org/index.php/Beginners%27_Guide). If you want all the shortcuts and his special config files, pick up Ev's [Build](http://build.evbogue.com). 

Then, if you have a buddy and you think they'd be willing to hang around during the install, invite them over. Pour beer or whatever you drink all day as you do something challenging.

Sometimes, depending on your current system, it'll take a couple of tries to get it to take. I'm on an Intel PC for the first time since 2000. It took a lot easier on this machine than it did on my Macbook Air (which is now bricked and belongs to the highest bidder who got it off ebay).

If that last parenthetical didn't scare you off, it should. You don't want to go fraking around with Arch if you have no idea what you're doing and you want to be certain of success. Success is not guaranteed. But then, the risk is part of what makes using Arch such a reward.

#### Who Else?

The other people who use Arch include [Dominic Tarr](/dt) and [Justin Orvis Steimer](/compersion). 

I know, because I helped them get it on their boxes, in person (Dominic, in California. Justin, in Mexico).

<div style="font-size: .8em">Updated 2014-02-08.</div>

