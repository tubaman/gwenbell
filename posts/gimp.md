---
title: Off Adobe, Onto GIMP
date: 2014-01-22
---

GIMP can do anything Adobe used to could do, and cheaper. By cheap I mean free. Gratis. 100% free, no monthly payment plans required.

#### My GIMP Workflow

1. I take the photo. I take lots of shots to get the one I want, and often, I take them on my hands and knees
1. When I'm ready to edit I pop the SD card into the slot on the side of [my machine](/arch) 

then do

	lsblk

lists the disks and the partitions.

1. I choose the Leica SD card with 

	sdc1

The full command is

	sudo mount /dev/sdc1 /disk

Then I'm free to navigate over to disk and tycat around.

1. I tycat the title of the file to preview it. tycat is a Terminology-specific command.

Then, when I've chosen the one I want to work on I 

	gimp [filename]

open the photo I want to edit, using tab to complete the filename.

Once in GIMP, the real magic begins. Not that all that navigation wasn't magic.

I should preface this by saying I never got pro at any of the Adobe products, and since their hack I've deleted the account I had with them. Anyway, I don't need them anymore. I have GIMP. And I live with someone who used to edit photos for NYMag...hundreds of them a day...and even he prefers GIMP over Photoshop. Why? I just asked him. "Because Adobe doesn't have its users best interests in mind. And GIMP doesn't have to worry about that."

The [tinies](/tinies) approve!

<div style="font-size: .8em">Updated 27 January 2014</div>
