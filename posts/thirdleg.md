---
title: The Third Leg
date: 2014-01-04
---

My third leg. I referenced it in [the last letter](http://gwenbell.com/handcraftedlist). One. Programming. Two. Publishing.

Three.

Painting.

I never ever ever thought I'd pick up a paint brush. Let alone days in a row. Let alone alongside one of the most wonderful people I've ever seen paint.

I want to send public gratitude to the two men I spent the winter holiday with in 2013. 

[Ev Bogue](http://evbogue.com) and [Justin Orvis Steimer](http://justinsteimer.com). 

They were patient, funny, smart, smart-alecky. All that, even as I [launched](/launch). They love each other and they extended that love to me. And somehow, during all that, whatever tiny desire I had in me to paint grew into a flame and at some point I look at Justin and Ev and go, "aww, fuck. Am I going to paint?"

And I did.

And then, 2014 started. I dedicated my painting career to the faeries, for 2014.

I know. 

Totally. 

Bananas.

***

I met Justin for the first time in 2011. Ev had said there was this secret magical place I wouldn't believe unless I saw it. The Schoolhouse in Brooklyn. I was heading to New York City for a Nintendo gig. I slept in Justin's bed one night, early 2011. I did not actually sleep. At all.

I thought about sleeping, and I knew then that The Schoolhouse was the place -- everything turned on its head.

I went back in 2012 to recuperate. I went back again in 2013 to deepen.

I keep going back. Because I love love love fucking love The Schoolhouse. As I love the people who make it what it is. 

Magic.

***

Nobody laughed when I said, in December twenty thirteen, I'm going all in on this painting thing. 

When I said I'm trying to draw demons (daemons, for systemd illustrations. Also grubs. Also pitchforks. Also kernels. [You get it](/tech).) 

Justin did not laugh. He said, "be aware of all the things that can change & then draw a whole page of little demons." Then he cooked, meal after meal, filet mignon chicken noodle soup stock from scratch and Wutang vs The Beatles. Ev and Justin supported, with nonstop love, the launch of _Write Before You Die_.

Justin wasn't the only painter I spent the holiday with. 

But he _is_ the one that left with another piece of my heart.
