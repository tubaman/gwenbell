---
title: The Art of Graceful Leaving
date: 2014-01-16
---

<img src="/images/painting-in-progress.jpg" class="profile" style="width: 100%" />

#### If you wait for permission, you’ll never get it. 

The only way to leave is to muster the courage to leave. If you’re little, or grown up. Doesn’t matter. If you know now’s the time to leave, leave.

I know you know what I’m talking about. I know I don’t have to explain it to you. You can do it. You don’t have to make a big deal about it. If you’re needing safety, sometimes a sound strategy is to leave.

You can leave with grace.

I’m the best guide because I’ve done it without grace so many times.

#### The Art of Graceful Leaving

I know what you might be thinking right now. You’re thinking it’s time to leave. To end it, but you feel scared to do it. 

Maybe you’re worried upending one thing will throw everything into question. 

Feeling the fear and choosing to leave is the thing.

Before I understood how to beam myself empathy, I thought maybe spending time with a family would help me get my need for belonging met. I thought maybe attending conference after conference would help me get my need for belonging met. Buying this pair of shoes or that piece of furniture. Being part of a project or team even though I was having to fake enthusiasm to do so failed to help me get my need for belonging met. 

That time I put two balls of wasabi up my nose to get a laugh at a sushi restaurant. That time I got [married](/divorce).

The list...is...[long](/mistakes).

It’s funny on a surface level. Ha ha, that was not a sound strategy ha, but then I notice a sensation. 

Embarrassment or sadness bubbles up. Oh! I haven’t fully mourned the loss. I may not have completely celebrated what was. I definitely haven’t yet moved on.

That’s what this is about. It’s about leaving. You don’t have to get out a can of gasoline and a pack of matches or go to town with a Louisville slugger like in your favorite country song.

You can leave with grace. 

I’m the best guide because I’ve done it without grace so many times.

#### When You Know Better, You Do Better

And when you know better, you do better. Said Maya Angelou to Oprah Winfrey and from Oprah to me to you.

There’s only giving yourself empathy at the decisions you made then. There’s no undoing it, but you can stay in empathy.

#### The Three Elements of Leaving

Leaving happens in three parts. 

+ celebrating what was (celebration of life is a need)
+ mourning the loss (mourning is a need)
+ moving on (growth is a need)

There’s a finite number of needs each human, regardless of age, has. We need rest, food and love. We need connection, belonging, peace and autonomy. Watch a child interact with her parent, and you’ll see these needs, regardless of whether she has a grasp on the language her parents speak.

Feelings and needs are universal.

#### The Two Kinds of Leaves

There are three elements to leaving: celebrating, mourning and moving on. 

There are two kinds of leaves: fast and slow.

#### The Fast Leave: Plunge Experiences

There are two kinds of leaving. The slow kind. And the fast kind. The fast kind is called a plunge experience. It’s a holyf***ingsh-- the rug getting pulled out from under you.

When your mom dies. When your house burns down. 

Those are two instances from my life that were plunge experiences: wholly impossible to prepare for.

#### The Slow Leave

There’s plunge experiences. The middle of the night everything changing kind. The sudden, seemingly without warning kind.

Then there’s the slow leave. 

The year after year enthusiasm waning kind of leave. Or the decade after decade decay type leaves. You’re leaving, but you’re less cognizant until one day you look back and realize you’ve left. 

#### How to End It

You know how to end it. Just like you began it. You walked through the door, you walk out it. You powered it on, you power it off. You open it, you close it. 

It’s a choice you make. It takes bravery, confidence and empathy. I believe you can do it. It’s the only thing left I can offer you, and I know I don’t have to explain to you why. 

You can do it. 

And there’s no time like right now. (Here's that scarf interlude I told you about at the beginning. [Gaman](/gaman)sters help me braven up.)

<img src="/images/rainbowscarf.jpg" class="profile" style="width: 100%"/>

The thing about leaving that surprises me, even though I keep on leaving and I should know by now. The thing about leaving is how little I’m missed. 

How I saw myself as an integral part of that place, or "movement", or person’s life. How the people in the community reinforced my idea of myself. The newspaper articles, the requests for coffee to pick my brain. The sense that without me, The Leaver, things would just crumble. 

I perceived myself as a fulcrum on which the event hinges. On which this company grows. On which this cafe or person who works at it - depends. 

And then, there’s the leaving. A something in the nighttime - some major or minor plunge experience that changes everything for the person on the receiving end of it.

I thought it’d leave some imprint permanently, on the lives of the neighbors. Or maybe when I left the site someone would miss my absence. One of the 17,000 followers would send me an email when they noticed I’m no longer updating.

They don’t notice.

Nobody notices The Leaver.

That’s the hardest thing to swallow about leaving and maybe the reason we don’t leave of our own volition. 

Because only by leaving do we experience the sadness of - not _having left_ - but of _not being missed_.

gb

