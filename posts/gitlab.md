---
title: Gaman and GitLab: 30 Days on GitLab, No Hitches
date: 2014-01-07
---
<img src= "/images/gaman-gitlab.png" class="profile" style="width: 100%" />

I spent a month with GitLab. I commited every day during December 2013. I managed a distributed non-team based all over the globe, most of whom were using Git for the first time. 

I hit zero hitches. I would recommend GitLab, in its current state, to anybody. As the project - [Gaman](/gaman) - unfolded, I answered as many questions as I could [here](/gaman). 

<img src= "/images/gaman-gp.png" class="profile" style="width: 100%" />

Takeaways:

1. I love private repos for multi-person, distributed non-teams
1. I love private repos in general -- you're safer to make mistakes and learn. Free is nice, and they're free on GitLab at the mo
1. I enjoy the GitLab interface as of January 2014
1. I [deleted all my repos from GitHub](/gitnotgithub) and am redirecting folks to my [GitLab](http://gitlab.com/gwenbell/gwenbell) page
1. And who knows, maybe this year I'll learn darcs

<img src= "/images/gaman-angelique.png" class="profile" style="width: 100%" />

Heh.

[Upvote this piece on LamerNews.](http://lamernews.com/news/1344#up)
