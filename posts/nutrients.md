---
title: How Do You Reinvent Yourself?
date: 2013-12-18
---

A reader emailed with a lot more questions than "How do you reinvent yourself?" But I am not going to answer all of them. Why? Because I saw a photo on the reader's website where they'd blurred out the faces of everyone else in the photo but themself. Which made me wonder. Did this person do that to protect the other people's privacy? 

Or did they do it because the only person who mattered in the photo was them?

#### The Questions, All But One Unanswered

<mark>1. How do you reinvent yourself?</mark><del> When do you know it's time to do so? And
how do you dig yourself into a new niche?
2. Why the US after Japan and Mexico? How do you decide where to live? (I
have had health insurance in 4 different countries--ie, lived actually in 4
different countries--so it's tough for me to decide.)
3. Before you reinvent yourself do you look at the money-making
possibilities or do you just go with your gut?
4. Any regrets on living your life so publicly online? How do you reinvent
yourself with public past personas?
5. What keeps you going? How do you know you're on the right path?</del>

I'm following their lead, deleting what I don't want from the photo, and focusing on what I _do_ want.

What I did there was hit 

	<mark>How do you reinvent yourself?</mark>

and

	<delete>on the other questions</delete>

#### Is it providing the nutrients I need?

Is it providing the nutrients I need? 

That is the question I ask myself most often. Is this piece of software providing the nutrients I need? Is this project providing the nutrients I need? Does this piece of clothing provide me the nutrients I need? 

Is this story I'm telling myself ...

Is this person with whom I sleep ...

Is this food I'm eating right now ...

Is this country I'm living in ...

Is this approach to the problem I'm tackling ...

Are these people with whom I live ...

The second half of the question is ... providing the nutrients I need?

Let me tell you a story and then I'm going to teach you some more tech since everyone emailing me (most, anyway) keep going, "I looooove your personal memoir and I haaaaaaaate your tech teachings," well... too bad. If you're not getting the nutrients you need from my work, move on. Though if I start getting donations to gwen at gwenbell.com to the tune of, let's say $100 a piece to my PayPal account, I'll keep up the personal memoir.

Anyway, the story. Because "the nutrients" piece comes from her. She had long, dreaded hair, dark brown. She hopped freight trains full time. She met strange people and they sang songs under the moonlight to her mandolin.

I asked her once, "how do you know when it's time to leave a place?"

She lived out of a bag, too, but a mangier bag than mine. She tilted her head and went, "I leave when I got the nutrients I needed."

And then I didn't see her again because people in America stopped having patience for vagabonding dreaded girls who play mandolin. So she got on food stamps and that was the last we saw of her.

The fact that people with her skill set have to go on food stamps and subsidized housing in America? I think that's shit. In some countries you [pay taxes by donating a piece of your art](http://blog.foreignpolicy.com/posts/2010/04/22/mexican_throwback_tax_with_a_twist). Can you imagine what a different world it'd be if we did more of that and less fighting "wars"?

I'm going on long. 

One reason I ask about the nutrients, and this is a misunderstanding people have, is I never wake up and go Oh! The weather is great. TODAY IT IS TIME FOR A REGENERATION. 

Hell. No. I never wake up and go "it's time for a regeneration."

Regenerations are awkward. When I find myself in one I go, "ah shit. I'm regenerating." My clothes fit me weirder. My hair looks weirder. My face looks weirder. My fingers do weird things.

#### And That Is Why You Do Not Regenerate

Of course, you might say that you do, you do regenerate, sometimes, once in a while, when you're in the mood and things are totally stable and you're sure where your next paycheck is coming from. 

If you ask my how I regenerate, or how I know when it's time, or how I know when it's coming? That means _you don't regenerate_. Because you don't choose to regenerate, it chooses you. And it's awkward and I wish it on no one. 

Unless, that is, you want to grow. If you want to brick, don't regenerate. If you want to grow, you have to pass through Awkward Village on your way to Regeneration Nation.

#### Tech Regenerations

I stopped using Apple products when the slidy outy panel thing started happening. I went, "what the fuck is that slidy outy panel doing on the side of the screen? Monitoring everyone's conversations?" Apple bills it as a benefit. I bill it spyware.

It was 2012 when I got the guts to get off. 

I was hanging out with another vagabonder, Erik, who lives in his van. He's a sage of sorts. A technosage. He runs Debian on a computer someone gifted him. He said why don't you try Linux? I started with Ubuntu (to my mind, almost as bad as Apple) and now I'm on Arch, Apple seems a jumble of silly bouncy yellow faces and slidy outy spy panels.

IT WAS NOT ALWAYS TRUE that Apple was a shitshow of yellow smiley faces and slidey outy sidebar spyware!

Richard Stallman might disagree and say it's always been spyware. But I remember. I was wearing a suit [my grandmother'd](http://nunubell.com) bought me to go to Japan, so it was either turquoise, royal blue, or black. 

It was the Year of Our Lord 2003 and I worked in a shithole of an English school, NOVA, in Kokubunji, Tokyo. It was my first, and it was my only, real full-time job. I'd just graduated from college. I saw someone using a PowerBook G4 (iirc) and they were making a logo in their breaktime in that shitty horrible teacher's breakroom. This was back when you could actually make some cash on the side designing logos for people. I know, quaint.

Whatever they were doing on that screen, I wanted in on it. I paid cash for my first Apple laptop later that same year. Bought it at the Ginza Apple store (unless they hadn't opened yet, in which case it was a different Apple store).

Anyway, now I choose between tiling window managers. 

And it's important, this choice!

And my point here is something can go from nutrient-rich to abusive...fast.

#### Militant about Git

Yesterday in an interview [Obi called me militant about Git](http://gwenbell.com/obiokorougo/). 

I got a chuckle out of it. I don't see myself as militant but it's good to know he does. I am passionate about freedom. I'm passionate about privacy. I think it ought to be a given, but right now it's not. And you have choice in the way you conduct your digital business.

But yeah, [if you don't know Git, you don't know shit](http://evbogue.com/dontknowshit/).

I get nutrients from Git. I get nutrients using dwm, thank you suckless. I get nutrients being out of the mother country. I get nutrients from my always-being-refined [Linux stack](/stack).

How do I know when it's time to regenerate? When I'm no longer getting the nutrients I need.

You know the hardest thing to do? [Leave](/leave).


