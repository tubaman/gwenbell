---
title: Regenerate
date: 2014-01-29
---

<img src="/images/regenerate.jpg" class="profile" style="width: 100%" />

Have you ever seen Doctor Who? 

If so, you know what I mean by regenerations. I regenerate.

I may not be a timelord or have two hearts but, I regenerate _often_. 

Sometimes I'm embarrassed as I regenerate; it's awkward. There's a teenager-like-element to regenerating. It can take you by surprise and you never know what you're going to come out as on the other side. 

I'm not embarrassed about who I was, nor how I spent my time back then. She helped get me here. And here is rad.

<img src="/images/gwenbelldotcom-2007.jpeg" class="profile" style="width: 100%" />

That was this site in 2007. Seven years ago as of 2014. I was way into hedgehogs. I hadn't had a hedgehog as a pet at that point.

[Paul](http://paulsalamone.com) did the graphics for me prior to moving to Berlin.

<img src="/images/europe-gb.jpg" class="profile" style="width: 100%" />

I went to school at the University of East Anglia from 2001 to 2002. I didn't drink all those Zimas. I've never been much of a drinker. But Pete and his buddies? I can't speak for them. This is probably Scotland, Edinburgh. 2002ish.

When not at UEA, I was at UNC-Chapel Hill getting a degree in English Literature. I took zero programming classes. But I learned to [nut graph](/nutgraph).

<img src="/images/gb-teaching-japan.jpg" class="profile" style="width: 100%" />

After I graduated I moved to Japan to teach English and later open a yoga studio. That was 2003-2006ish, and I co-ran it from abroad 2006-2010.

Circa 2007 to 2011 I was paid by companies to be their [social web guru](http://www.fastcompany.com/1596274/gwen-bell-social-media-guru). I was featured on [the cover of a magazine](http://www.experiencelifemag.com/issues/may-2010/life-wisdom/well-connected.php). From 2011 to 2012 I traveled the globe. In that timeframe I got [married and divorced](/divorce).

In 2013 I spoke on [Git at The Flatiron School](http://blog.flatironschool.com/post/52299707175/guest-speaker-gwen-bell) in Manhattan.

I quit [Twitter](/tech) in 2012 and again in 2013. I had an absurd amount of "followers" but it was a drain. Also, it seemed to be dying. I wasn't getting [the nutrients](/nutrients) I needed. So I [left](/leave). To see the other things I've left, view my [tech page](/tech).

Before full-time social web strategizing, I owned a yoga studio in Japan for seven years.

During that time, Patrick and I launched Zen is Stupid and I co-founded Buddhist Geeks.

#### Bookography

+ The Scary House, the 90s (an Army base somewhere in the world)
+ Digital Warriorship, 2010 (Boulder, Colorado)
+ The Attention Detox (San Francisco, California)
+ Experience Telling Template & Tutorial, 2011 (Sayulita, Mexico/Seattle, Washington)
+ Discover Your Needs, 2012 (San Francisco, California)
+ Reverb, 2012 (Singapore)
+ Digital Warriorship, 2012 (second write published from Kyoto, Japan)

<img src="/images/theworkberlin.jpg" class="profile" style="width: 100%" />

+ Zeitguide, 2012 (Berlin, Germany. Work space in Berlin pictured above)
+ Git Commit, 2013 (Brooklyn/Manhattan, New York)
+ Terminal, 2013 (Brooklyn, New York)
+ Align Your Website, 2013 (Oakland/San Francisco, California)
+ Write Before You Die, 2013 (Mexico City, Mexico)
