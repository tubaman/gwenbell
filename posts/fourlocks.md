---
title: Four Locks
date: 2014-01-30
---

> Security codes on everything <br />
Vibrate so your phone don't ever ring <br />
(Joint Account)<br />
And another one he don't know about <br /> - Ciara, Like a Boy

<img src="/images/ileftamerica.jpg" class="profile" style="width: 100%" />

> I do not enlighten those who are not eager to learn, nor arouse those who are not anxious to give an explanation themselves. If I have presented one corner of the square and they cannot come back to me with the other three, I should not go over the points again. ― Confucius

It's 2014. I live in Mexico.

There are four locks between me and the street level of my apartment.

Four.

If you wanted to get to me right now you'd not only need all the keys and to know how to use them, you'd need to know what level I'm on, and what door I'm behind. There's a door person at the very front door, so you'd have to clear that border, too.

Sub-par locks means no privacy. A door is not privacy. The ability to reinforce that door is privacy. Privacy is a need, not a privilege. I know the rent is too damn highin America, but it's not just that. 

It's also the locks are too damn undependable.

It all strikes me as a microcosm of what's happening with "the NSA stuff" as people I know are now calling it. ["The NSA Stuff"](http://www.nytimes.com/2013/08/18/magazine/laura-poitras-snowden.html) as though we can't even call it what it is: the abuses of the government of the United States of America.

We can't call abuse abuse because then we acknowledge: **we have a problem**.

It's 2014. I live in Mexico. 

Now there are four locks between me and you. And that's how I want it. Not that you would ever "do anything" -- but I assume you will. And so did the person who architected this building. Americans, maybe we think we own the place. Maybe we think we own the whole house, even the bathrooms and the bedrooms, with someone else renting and living in them. Maybe we think those are ours, too.

Americans, the other half of us, maybe we're over-trusting. And maybe it's time to stop that. Before anybody else gets hurt any worse.

If it seems I'm all "[tech tech tech](/tech)" all the time, it's because I am. I stay ahead with my operating system and everything on it to stay safe(r). I'm adding more locks for my digital self.

A few of you have asked if I still do yoga. Now that I have four locks between me and the street, I do my practice again and take an actual breath.

Oh yeah, I can sleep at night now, too.

I'm the leaseholder. I'm the only one with the keys to the locks. 

It's not a metaphor. It's an indicator. If it seems as though the government is all up in every aspect of our lives, think about it. Maybe it's because we didn't put enough locks on the doors, and/or maybe we did, but then we gave them the keys to the kingdom. I hold the keys to this kingdom. [Digital](/lsnotclick) and otherwise.

Maybe you want to, too.

Maybe start with [Arch](/arch).

And hey, you might wanna lock the doors first.

***

One of the [150](/contact) got in touch to say you love [Leave](/leave) and asked about the book on addicition I've mentioned.

It's [Anne Wilson Schaef's](http://annewilsonschaef.com/) _When Society Becomes an Addict_. [It's on my appreciations page](/appreciations) with a few others I treasure.
Here's a decade-old [evaluation of the book](http://everything2.com/title/When+Society+Becomes+an+Addict). The author of the review, oakling, writes:

> Choosing to brave the unknown, to try living as honestly as possible, is the definition of recovery. It is also the way out of the abusive system in which we live.

<div style="font-size: .8em">Published 2014-01-25. Updated 2014-01-30</div>

