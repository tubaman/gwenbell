---
title: Thanks to Brian and Scott
date: 2003-07-10
---

<mark>My first blog post. To [LiveJournal, July 2003](http://gwendolynbell.livejournal.com/477.html). As luck would have it, I thanked the baristas at [Driade](http://caffedriade.com/), my favorite cafe. In the world. Driade's where I first heard the word "blog". Don't reckon those guys could have imagined then the monster they'd helped birth.</mark>

For my first posting, I'd like to thank the fellas down at Caffe Driade for leading me to this site. Without their help, who knows what trouble I might be getting into right now! For three bucks you can get the best iced vanilla latte in town, and some hot computer tips to boot.

<div style="font-size: .8em">Added 2014-02-07</div>

