---
title: Stack Review January 2014
date: 2014-01-26
---

<img src="/images/meishi.jpg" class="profile" style="width: 100%" />

It's January for only a few more days. 

Let's check in on [Jan stacks, shall we](/tech)?

### 1. The Tech Stack

1. Arch Linux. I love knowing my machine insideout. Along with [Ev](http://evbogue.com) I've now helped, in person, two fellow technologists (well, one painter, one programmer) get Arch on their boxes. Justin, while he was here in December, bought a tiny Acer just for the purpose of Arching. I was, to be honest, a little jealous, since he rolled back to [the Schoolhouse](/compersion) with a Spanish keyboard.
1. Haskell. Today I'm diving deeper into Haskell and [pandoc](http://johnmacfarlane.net/pandoc/). Something about Haskell just clicks in my brain. I love using and growing my site with [Hakyll](http://jaspervdj.be/hakyll/) on a daily basis.
1. Email. I still use and enjoy [Mutt](/mutt), a command line tool.
1. Git. Par usual.

### 2. The Paper Stack

1. Yesterday I made business cards for a gathering with other artists. I gave away two cards, including the one up top!
1. Fabriano. This started with someone gifting me one of his Fabriano notebooks. Thanks to the gift, I now buy and paint on [this paper](/tech) only.
1. I wish I could find Palaminos by California Republic in the city. I'd buy three. I would love to end my relationship with Moleskine.

### 3. Stack Fuel

1. Cielo café, Venezuela
1. Queso de Oaxaca
1. Mornings in the mercado, with which I have a date in five minutes.

What's your end of January [stack](/tech)? [Is there magic in it](http://evbogue.com/themagic/)?

Let me know [in my Muttbox](mailto:gwen@gwenbell.com). And, as ever, [IRC](/irc).

