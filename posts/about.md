---
title: About
date: 2014-02-14
---

<img src="/images/hello.jpg" align="right" class="profile" style="width: 100%; margin-bottom: 1em;" />

Hey, I'm Gwen Bell. 

As of 2014 I live in México, eat a lot of piña right off the cart, tons of pig tacos and other amazing food. I also run around the city when not working on the command line. I'm living my ideal life. Which, yeah. I know I'm lucky. Also, I am ruthless with cutting the fat. That's _why_ I'm lucky. I make room for luck.

I believe there's so much living to do in one set of 24 hours. I believe you can live [so many lifetimes](/regenerate) in one lifetime. 

I learned Haskell so I could build this site and I compile it with

	ghc --make site.hs

but I haven't memorized my home address. That's the truth.

I **do** know how to get home. I just think it's useless to memorize my home address, so I wrote it down somewhere and forgot about it. 

I think the world will be a better place when the internet is free again, when governments respect our privacy and when a kid can visit the doctor without fear a hacker might later access all his personal bits. I add links about this kind of thing to my [appreciations](/appreciations) page.

<img src="/images/gb.jpeg" align="right" class="profile" style="width: 100%; margin-bottom: 1em;" />

#### Tech Background

I've been in tech, contributing to tech projects, since 2006. 

It started in Japan. That's where I taught myself to code, while my two red eared sliders sunbathed. It was during that time that I saw an advertisement for a social network inviting me to come "change the world." The ad was full page, at the back of in an issue of Yoga Journal on which I'd spent what was to me at the time an ungodly amount of yen on just to connect to the American yoga scene. 

That time period I talk about in my [Zaadz piece](/zaadz). I became a Zaadz ambassador, and it was one of the earliest social sites in which I engaged, publishing writing, interviewing other participants. I LOVED the crossover ... the on-the-mat/off-the-mat technology/upward facing dog-ness of Zaadz.

Then Gaiam acquired it (Gaiam is a huge manufacturer of plastic yoga garbage) and they threw the color purple all over this golden hued orange site that I'd helped grow into something beautiful. I'd met many of the founding members of Zaadz during a trip to California in 2006. That was my first from the internet to in person meeting trek; I met [Obi](/obiokorougo) in a sauna that year. Gaiam shut it down. At the time I was still on the books as a contracter, doing design work.

So, that was the yoga meets tech thing. The first website I built myself was emptycup.org, Zen feel, hand built at night, I taught kids English during the day at Yamamae
Sho. My second site was Yoga Garden's home page, the site for my yoga studio.

Japan was '03, '04, '05, '06.

During that time I podcasted a ton: Zen is Stupid, then later Buddhist Geeks. You'll start to see more of my podcaster/[interviewer](/zedshaw) side reemerge, now that I'm out of America and have my [four locks](/fourlocks). I have my privacy and security needs met again. 

**2012 and 2013** I published digital books and courses while traveling the globe.

**2011** I got [divorced](/divorce).

**2010** I was on the cover of a magazine and featured in FastCompany, Forbes, among others, for my work. I ran a boutique social web firm.

**2009** I got married and the whole thing was live-tweeted.

**2008** I worked for TechStars.

**2007** I started a lost a design firm to a [fire](/fire). I taught yoga at the first Startup Weekend.

I am always working towards [alignment](http://align.gwenbell.com). To that end, my work and this site [regenerates](/regenerate).

Gwen Bell <br />
_Mexico City, México_ <br />
_February 2014_

<div style="font-size: .8em">Updated 2014-02-14</div>

