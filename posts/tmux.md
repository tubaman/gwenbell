---
title: Tmux
date: 2013-12-12
---

<mark>This is a companion piece to [IRC](/irc)</mark>.

Tmux means terminal multiplexer. I think that's such a funny name. It sounds so ... magnificent! When really all it's doing is monitoring your [IRC](/irc) channel when you're not in it.

#### Why Would I Tmux?

You would Tmux if you'd like to go to sleep or eat coffee ice cream out in the world, and have the IRC channel you're in be available to read once you've awoken or returned from ice cream. 

So for [#gaman](/gaman), I attach Tmux, fall alseep, wake up, and do merge requests. No problem. 

Without [Tmux](/tmux), I have no idea you visited the room, nor what you said. With Tmux, I see what you said, when you said it. Magic or what?

#### How Do I Tmux?

First, you gotta have a computer that never sleeps. 

Mine is an [Arch](/arch) instance on [Digital Ocean](https://www.digitalocean.com/?refcode=14a7846491d4). That's a rewards link.

Now that you've got your instance up you can set up Tmux.

If you're on an Arch instance, it's as simple as

	$ sudo pacman -S tmux

Then, to start a Tmux instance when you login, do

	$ tmux

Then start Irssi within tmux

	$ irssi

Now is your chance to log into your favorite IRC channels in Irssi. Start with #gaman! Fewer trolls.

Then, to detach, you do

	$ ctrl-b

Then

	d

Which detaches you. Now, you're using Tmux, your instance will stay awake even as you sleep, and when you wake up in the morning you do 

	$ tmux attach

You'll see what you missed while you slept/ate ice cream. Repeat the process when you want to detach again. If you don't detach that way, nothing will be there when you return.

Questions? [Come into](/irc) #gaman and ask!

<div style="font-size: .8em">Updated 27 January 2014</div>

