---
title: 5 Questions with Roslynn Tellvik: For the Love of Gaman
date: 2013-12-19
---

**RT:** Gwen, Still on the gmail. For now. 

My grandma died on Monday - peacefully, after four days in the hospital, three of which I spent with her and our entire
family - I love her supermuch and that love reminds me how precious each little tiny color and taste and texture and sound and moment of presence is. Thanks for asking the questions. Thanks for presence.

**GB:** What are you working on right now and how's that going for you?

**RT:** Working on RAFT botanical cocktail and soda syrups. Brewed a batch of hibiscus lavender last night. Bottled up 158 wee sample bottles and 12 regular sized bottles. That makes three flavors brewed and bottled and waiting for labels. The labels are delayed by a clunky flunky printer. I was mad at first. Then sad because we had to reschedule our launch house party. Turns out my Grandma went to the hospital two days before the original party date and died three days later. I was meant to be there. 

The flunky printer just smoothed the way. Now we'll get labels in time for our launch party this weekend. Once we get labels our friend Leila will shoot the bottles in her lightbox with a heavy camera and we can put the pretty pictures on our website.

Copywriting is haaaaard. I'm not sure how to feel about it. When I write it I cringe. When I hire someone to write it I wince. When I read other people's copy I generally don't experience that critical intensity. Even when I don't enjoy the copy, it's rare that I actively dislike it. Anyway, I will get over it and probably put up some copy that makes me cringe for the love of gaman. In time resistance will move on to other battlefields. I don't like my face on the internet. I'm putting it there anyway. I love creating recipes to make drinks with the syrups.

**GB:** How'd we cross paths?

**RT:** I believe it was way back in 2010. Twitter or some blog. I clicked to your website to try and meet my need for validation and belonging. I felt as
alone as ever after visiting. That nearly always happened in those days. I was trawling the interweb for someone to tell me/show me/direct me what to
do. I thought I wanted to take my coaching career independent and start an online business. Down the infoproductrabbithole I went. But back to your
website - I remembered it because it didn't feel phony or trite. You struck me as a badass. I remember thinking your yoga studio in Japan adventure was
awesome and I wanted to hear more about it. I lived in Japan for a year in the mountains of Okayama. One of three places I have spent extended time
that will always occupy my heart. I must have signed up for your email list at that time in the hopes that I could learn to summon the kind of confidence you displayed online to my own online pursuits. Glad I did.

**GB:** What about the work I'm doing right now resonates for you?

**RT:** I resonate with the bridgey nature of your work. 

I experience your writing and [emails](/handcraftedlist) as a great big call to act. To change my relationship to technology. To notice how I have allowed it to replace personal engagement and critical reasoning and to experiment with reclaiming my own interest in how things work. On one end of the bridge: operating systems, profit platforms, user interfaces, consumption, compliance, vacancy. On the other: the command line, commits, production, generosity, presence. Your technical assistance work is the bridge. I've only begun to approach the span. 

At times I feel tentative, hesitant, uncertain...will the bridge hold? Will I lose all my data? Will I f*** it up somehow in my ignorance? Then your
writing somehow responds: yes you can lose it all, you probably will f*** it up. And you'll be better for it. Life will be better. I believe you. So thanks for putting it out there. 

**GB:** What does 2014 look like for you?

**RT:** Well...let's see (slides palms together with anticipation). A focus on movement. Bodily strength, presence, grace, and engagement. Moving the
body, moving the pen, moving the stagnant stuff on outta there and welcoming the fresh stuff on in. In 2014 I see myself claiming more space.
This looks like breaking up with the operating system. Convening a class on bodily strength, presence, grace and engagement. Kicking ass at my day job and in my business. Laughing more. Saying no. No, really...no. I don't seem to have trouble saying yes, but in 2014 I see myself being more discerning about yes. Saying bigger yesses and only when I mean it.

**GB:** What was your biggest lesson learned from a mistake in 2013?

**RT:** In 2013 I made the mistake, many times, of measuring decisions based on what i thought other people wanted and needed. This stung on a couple of
occasions as I either guessed wrong on those otherpeopleswantsandneeds (though guessing right is not much better, really) or compromised my own boundaries by making decisions that way. I noticed this didn't strengthen relationships, but weakened them. 

It will take practice and gaman suru-ing to shift focus. It's worth it. I learned that I am a happier person when I am making decisions based on my needs and wants. I have more energy, like people better, and end up with extra space in my head for creativity and visioning positive stuff. 

And all that holds true even when what I decide contributes to other people feeling unhappy or mad or disappointed. I am so
grateful to have noticed this in 2013. Looking back I can see how this mistaken focus on otherpeopleswantsandneeds has plagued me and my
relationships for years. It's a tiny taste of freedom. More, please.

