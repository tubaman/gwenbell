---
title: 5 Questions for Robyn Devine: Of Hats and Babies!
date: 2013-12-16
---

In today's [letter](/handcraftedlist) I told you I'm doing an interview experiment. I asked for responses to five questions. 

An interview! Because I love asking interview questions, and I love being on the receiving end of interview questions. Just in the last week I've participated in three: [On Money and Making Do](http://nunubell.com/stories), [If You Don't Know Git You Don't Know Shit](http://evbogue.com/dontknowshit) and [Website Alignment with Gwen Bell](http://www.braidcreative.com/blog/website-alignment-with-gwen-bell).

You're sending your responses!

#### Robyn Devine Makes (Lots of) Hats!

**GB:** What are you working on right now and how's that going for you?

**RD:** I'm making hats and giving them away, sharing the process at [She Makes
Hats](http://www.shemakeshats.com).

My focus for 2014 is going to be on donating to a specific charity each
month, while also trying to make/collect 2,000 hats for local kids who live
at or below the poverty line.

I'm also in the final stages of editing a book (about making hats and
giving them away, of course!) that's set to come out this spring!

**GB:** How'd we cross paths?

**RD:** I've read your blog for a few years now, and last winter I sent you a pixie
hat to help keep you warm in Colorado.

(Note from Gwen: Robyn, thank you for the hat. At the time I was in Kansas City, watching the Google Fiber rollout and setting the stage for 2013, during which time I pulled off all Google products and deemed them harmful.)

**GB:** What about the work I'm doing right now resonates for you? What about it has you fired up? What about it has you totally lost? What about it infuriates you? How can I improve your experience of the work I do?

**RD:** I love to read about the work you're doing!  What most resonates with me is
aligning my website to my real-life self, something I'm trying to focus on
at least a few times a week since picking up your latest offering. After my
first go-through I radically altered the look/feel of my site, pared back what I shared online and what I focused on, and am far happier for it.

I don't always resonate with the tech sides of your writing, mostly because
I don't give myself the space/time to dig into what it would mean to get in
deep with the tech side of this online world. Right now I'm totally at
peace with that, and so focus on what I am getting from your writing.

**GB:** What does 2014 look like for you? What major changes/breakthroughs do you see yourself making in 2014?

**RD:** I plan to knit hundreds more hats! The biggest change in my world will be
the birth of our baby girl in early March, and I'm excited to see how that
shapes and changes the way I live my passions (wife, mama, hat maker). I
know that will change the way life looks, but I'm excited for that change,
and can't wait to figure out how to keep making hats with a newborn (along
with our toddler son!) in tow!

**GB:** What was your biggest lesson learned from a mistake in 2013?

**RD:** My biggest mistake in 2013 was focusing too much time on selling hats, and
not enough time on giving them away. While it's nice to make a few bucks
here and there off the hats I make, it's pretty clear that my passion is
for giving them away, so I'm going to leave selling hats to other folks
from now on for the most part.

#### Fire and Quirk

Robyn, thank you for your responses to the interview questions. I love the fire and quirk in your work! PS: If I order [this one](http://www.etsy.com/listing/166857287/ready-to-ship-chunky-bonnet-purple-pink?ref=shop_home_active), does it ship with a sleeping baby?
