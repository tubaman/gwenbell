---
title: 31 Days of Commits
date: 2013-02-25
---

<div style="font-size: .8em">Updated 27 January 2014 to add I now push to [GitLab](https://gitlab.com/gwenbell/gwenbell).</div>

Today marks 57 (it was 31, now it's 57) days in a row I've made at least one commit to git, and pushed it to Github, and then live to my site.

####Insights

+ I work [modularly](https://npmjs.org/)
+ I began recognzing strong developers by being in code each day
+ As with yoga, a little each day is better than nothing and then tons
+ [Life changer: Git Aliases!](http://tjholowaychuk.com/post/26904939933/git-extras-introduction-screencast)
+ Deploying trumps talking about deploying
+ Deploying leads to self-reliance
+ I am now both a [writer and a hacker, like Paul Graham](http://www.paulgraham.com/hp.html)
+ When not hacking, I [dance](https://myspace.com/gwenbell) or stretch or nap or read
+ Am. Constantly. Tweaking.
+ Learned the command line by moving over to Linux
+ <del>I'm back on Mac OS X after 6 months on Ubuntu - nice to be back, but</del> I plan to be Linux full-time eventually. Updated 23-01-2014: Am now on [Linux full-time](/arch).
+ I wish everything could be git committed. Git has changed my life.

<div style="font-size: .8em">Updated 2014-01-27</div>

