---
title: The Git Magic Mind Map
date: 2014-02-04
---

I met with a mentor earlier this week. By earlier this week I mean yesterday. Lots happens in one day in the gb world. 

He's a famous artist in Mexico. I came home and combined my three loves: computing, writing and painting.

**THE GIT MAGIC MINDMAP**

<img src="/images/gitmagic.jpg" class="profile" style="width: 100%" />

I learned about mind maps during yoga teacher training. Sevenish years ago.

Now I'm adding color to mind maps. [Paint](/thirdleg). The first map I wanted to make is the first one I made: Git. A love affair that grows and becomes more resilient each day.

As I wrote about in [Git Commit](http://git.gwenbell.com/), it changed everything about the work. I know my writing and code bases have cryptographic security built right in. I wish everything were Git committed. I think the world would be a more honest place.

In the meantime, here's some 

	--git magic

¡Buen provecho!


