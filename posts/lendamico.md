---
title: 5 Questions for Len Damico: You're Not Afraid to Throw A Match and Start Over
date: 2013-12-20
---

**GB:** What are you working on right now?

**LD:** The mobile web: lots of "hey, we have this site, and we want it to look good on my phone." Everything from content strategy => wireframing/prototyping => design => development. [Len Dami.co](http://lendami.co) for now; in the process of realigning.

**GB:** How'd we cross paths?

**LD:** A Spring 2011 Nintendo Brand Ambassadors event in Seattle. We sat next to each other on the bus shuffling us to another event. I made some shitty comment about your homemade felt nametag.

**GB:** What about the work I'm doing right now resonates for you? What about it has you fired up? What about it has you totally lost? What about it infuriates you?

**LD:** Strangely enough, the answer to the first four questions is the same: The capricious nature of your work. You're not afraid to throw a match and start over, which can be both extremely liberating and maddening. I’m a recovering packrat, so I need to see that example in others.

**GB:** How does 2014 look for you? 

**LD:** I'm working hard and laying the groundwork to make 2014 about autonomy, agency, boundaries and finding my voice.

**GB:** What was your biggest lesson learned from a mistake in 2013?

**LD:** "If you find water rising up to your ankle, that's the time to do something about it, not when it's around your neck." – Chinua Achebe


