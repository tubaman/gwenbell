---
title: 5 Questions for Obi Okorougo: The House Was Just a House
date: 2013-12-17
---

Obi is one of the newest subscribers to the letter even though we've known each other for years. We lost touch for a few years because I deleted my FaceCrack account and he didn't. Yesterday I looked at his website, [theuberman.com](http://theuberman.com), and saw it'd been hacksorcized. I emailed Obi about it: dude, your site's been hacksorcized. This morning he emailed back, said indeed it had. Within 24 hours he got it straightened out. That's the kind of guy Obi is.

If only reclaiming a house that belongs to you were that easy. This interview broke my heart.

<mark>Edited later today, the 17th. I cut the first half of the interview to focus on the second half, where it gets honest.</mark>

**GB:** Obi. How'd we meet?

**OBI:** We met on a social network that we both used to work for - digitally at least. We met in person when you crashed at my barren pad in Los Angeles. We toured the city in my champagne Solara, and spent many fun nights eating bachelor-style stir fry with chopsticks that you forced me to use, and doing half-naked interviews in saunas. We had deep conversations with tears, drunken nights at the Geisha House in Hollywood, and another strange experience in Santa Monica that I won’t mention.

That was quite an exciting few days, G.

I resonate with your passion—that’s something I always admired about you. I can tell that you’re really juiced about Git and your militant approach to it sparks up a part of me that feels all of life should be approached with the same fire. At the same time, another part of me feels like it’s some secret society that I’d be dumb not to be a part of—exclusive? Something. I can’t say that it infuriates me though. There’s a part of my brain that shuts down when things become too cerebral though, and I notice that happening when I read certain posts about it.

But that’s just all about how my brain works. You just keep doing you.

**GB:** How does 2014 look for you?

**OBI:** 2014 is going to be a culmination of everything I’ve learned and experienced in the past decade. I see it; I feel it; I’m already riding the wave into it. 

I’m a full-time expat for the first time in almost 4 years. And this time I was not invited, I just left. 

There is a solitude that I’m getting used to. And a freedom that I’m enjoying. There have been things that I’ve put off and this coming year I’m embracing them, holding them close, taking what’s necessary and dumping the rest. I see this year as a shedding of skin, with a new man emerging — all loving and intense and crazy and real.

**GB:** Biggest lesson learned from a mistake this year?

**OBI:** In May of this year, I lost the house that I grew up in to foreclosure. It was my Mother’s house. She was really proud of it because she bought it on her own. It had sentimental value because it was the first place we lived in after my father passed. I lost my virginity in that house. My mother died in that house.

I traveled to Northern California in an attempt to work with lawyers and get the house in my name so that my sisters and I could decide what to do with it. Through some lawyer negligence, and my inexperience dealing with the fuckers, we lost the house. I felt bad — I made it my responsibility to save it. And I failed at my job.

I learned a few things. First, detachment. The experiences I had in the house were not left there, they are still with me, and no bank or barrister can
take them from me. The house was just a house.

Second, do the work even it frightens you. 

It was really emotional having to live in the house while working with lawyers on my Mother’s estate. I trusted a bit too much and ignored some things I shouldn’t have. Some things fell through the cracks. I then vowed never to let fear, sadness, or pain keep me from the work. You show up. You just do— everything and everything and everything.
