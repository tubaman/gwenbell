---
title: Appreciations
date: 2014-02-13
---

+ [Our Newfound Fear of Risk](https://www.schneier.com/blog/archives/2013/09/our_newfound_fe.html)
+ [We're Being Enslaved by Free Information](http://spectrum.ieee.org/podcast/computing/networks/jaron-lanier-were-being-enslaved-by-free-information)
+ **Required viewing** [Through a Prism Darkly](http://cdn.media.ccc.de/congress/2013/webm/30c3-5255-en-de-Through_a_PRISM_Darkly_webm.webm): Minute 28:26, Why MetaData Matters, has haunted me for days. You gotta consider the ramifications of people avoiding self-care for fear their call or search is being tracked.
+ So remember that [IRC](/irc) tutorial I gave you? [IRC Networks Under Systematic Attack from Governments](https://www.quakenet.org/articles/102-press-release-irc-networks-under-systematic-attack-from-governments). Appreciate Kurt's cogent, and somehow, lighthearted, approach to this. Clarifying.
+ [Is the U.S. Congress Arming the Very People Who Will Commit the Next Attack on U.S. Soil?](http://ronpaulinstitute.org/archives/featured-articles/2014/february/02/the-continuing-al-qaeda-threat.aspx). Ron Paul. See also: Syriana with George Clooney. Provocative.
+ [LYAH](http://learnyouahaskell.com/chapters) 
+ BJ Mendelson's seminal Social Media is Bullshit. Lucky for me, I knew it already, and got out the social game prior to its publication. Uproarious. Honest.
+ I've always opted out, except once, pre-"I opt out" languaging. Even though it slowed me down at the airport, and made already disenfranchised employees peeved.[Now I'm happier than ever with that decision to **opt out**](http://www.politico.com/magazine/story/2014/01/tsa-screener-confession-102912_full.html#.UuvJb1PLdIU).
+ "It’s perhaps the most effective means of defeating the online surveillance efforts of intelligence agencies around the world, including the most sophisticated agency of them all, the NSA. That’s ironic, because Tor started as a project of the U.S. government." - [Bloomberg Businessweek on Tor](http://www.businessweek.com/articles/2014-01-23/tor-anonymity-software-vs-dot-the-national-security-agency#r=rss)
+ [Bruce Scheneier, thank you](https://www.schneier.com/).
+ "There’s no way for readers to be online, surfing, e-mailing, posting, tweeting, reading tweets, and soon enough doing the thing that will come after Twitter, without paying a high price in available time, attention span, reading comprehension, and experience of the immediately surrounding world. The Internet and the devices it’s spawned are systematically changing our intellectual activities with breathtaking speed, and more profoundly than over the past seven centuries combined. It shouldn’t be an act of heresy to ask about the trade-offs that come with this revolution." - George Packer, [The New Yorker](http://www.newyorker.com/online/blogs/georgepacker/2010/02/neither-luddite-nor-biltonite.html)
+ [Well-curated NSA Source Documents](http://freesnowden.is/revelations/index.html)
+ [Brendan Eich, Trust but Verify](https://brendaneich.com/2014/01/trust-but-verify/) Indeed.
+ [Laura Poitras in New York Times Magazine](http://www.nytimes.com/2013/08/18/magazine/laura-poitras-snowden.html). Brave.
+ [Zed Shaw, Rails is a Ghetto](http://harmful.cat-v.org/software/ruby/rails/is-a-ghetto). Uproar.
+ [George Orwell, Politics and the English Language](http://harmful.cat-v.org/words/politics-and-the-english-language): "Political language -- and with
variations this is true of all political parties, from Conservatives to Anarchists -- is designed to make lies sound truthful and murder respectable, and to give an appearance of solidity to pure wind." Smart.
+ [Voluntary Simplicity](http://duaneelgin.com/books/voluntary-simplicity/): I live out of one bag. Duane Elgin lit the fire under my ass (with his book) in 2000. Simple.
+ [How to Ask Questions the Smart Way](http://www.catb.org/~esr/faqs/smart-questions.html): Right.
+ [When Society Becomes an Addict, Anne Wilson Schaef](http://annewilsonschaef.com): Liberating.
+ [On the Shortness of Life, Seneca](http://www.us.penguingroup.com/nf/Book/BookDisplay/0,,9781101651186,00.html?On_the_Shortness_of_Life_Seneca): direct.
+ 1984: beyond comment. Read it for the first time in November 2013; world-view changer. Provocative.
+ [Japanese Death Poems](http://www.alibris.com/Japanese-Death-Poems-Written-by-Zen-Monks-and-Haiku-Poets-on-the-Verge-of-Death-Zen-Monks/book/10623407).
+ [Thoreau 2.0](https://static.pinboard.in/xoxo_talk_thoreau.htm): Brave.
+ [ANVC: Almost Nonviolent Communication](http://anvc.svenhartenstein.de/en/about/): Gets communication. Right-ish.

#### Music & Podcasts

+ I co-founded Buddhist Geeks and before I bounced, I interviewed my first actual teacher, Fleet Maull. He's now an acharya or somesuch. (I'm more Stoic than Buddhist now, and more technologist than Stoic.) This piece on plunge experiences, though, stays with me to this day: [Where the Rubber Meets the Road](http://www.buddhistgeeks.com/2007/03/bg-010-where-the-rubber-meets-the-road-fleet-maull-on-plunge-experiences/). Legit.
+ Sigur Ros

#### Cine

+ Tron: If the future turns out to be this? Rad.
+ Y Tu Mamá También: I've watched it in Spanish only. So I didn't understand all of it; that said, sex scenes are language-agnostic. Climactic.
+ A Series of Unfortunate Events: Sometimes my life is a series of unfortunate events in my head. Poignant.
+ American Beauty: One of my all time favorite films. Honest. Brutal.
+ [Every episode of Six Feet Under](/comeoutclean): Thank you Alan Ball. Honest.
+ [The Big Lebowski](http://www.rottentomatoes.com/m/big_lebowski/): Dude.
+ [Heima](http://www.heima.co.uk/): Sublime.
+ [Moonrise Kingdom](http://www.rottentomatoes.com/m/moonrise_kingdom/): touching film, smart plot and actors. Smart.
+ [Pan's Labyrinth](http://www.rottentomatoes.com/m/pans_labyrinth/): amazing, gorgeous, stunning, heart-moving movie about little girls, faeries and war - perhaps my favorite film. Moving.
+ Kung Fu Hustle: over the top humor. Clever.
+ The Dictator: some things are beyond comment. This film is one such film. Outrageous.
+ Babel: everything I love in a film -- I've [lived in every place in it](/expat), including the High Atlas Mountains in Morocco, Japan and Mexico. Deep.

#### Love, Love, Love, Love, Love

+ [Justin Orvis Steimer](http://justinsteimer.com): for teaching me to paint, December 2013. Legit.
+ [Ev Bogue](http://evbogue.com): tells me the truth every single time. Even when I don't wanna hear it.
+ My grandparents, [Nunu & Bub](http://nunubell.com): Number one influence of all time. These two have not just influenced my work, they've helped me as I've shaped it. I am in their debt, forever. (Not that they expect me to pay it back...).
+ Buckminster Fuller: inspired the Gwendolyn Round House. **Source documents:** the original in [2000](/images/2000-gwendolynroundhouse.jpg), updated by an Italian architect named Roberto in [2013](/images/2013-gwendolynroundhouse.jpg). Futurist.

> We must do away with the absolutely specious notion that everybody has to earn a living. It is a fact today that one in ten thousand of us can make a technological breakthrough capable of supporting all the rest. The youth of today are absolutely right in recognizing this nonsense of earning a living. We keep inventing jobs because of this false idea that everybody has to be employed at some kind of drudgery because, according to Malthusian-Darwinian theory, he must justify his right to exist. So we have inspectors of inspectors and people making instruments for inspectors to inspect inspectors. The true business of people should be to go back to school and think about
whatever it was they were thinking about before somebody came along and told them they had to earn a living. - Buckminster Fuller

+ [Dr Bronner](http://drbronner.com): mid-2013 I read the fine print on the peppermint Bronners soap I prefer (the only cosmetic you need is not an understatement, it's a life choice). Reading the fine print I discovered Dr Bronner's family was killed during the Holocaust. Bronner'd already fled, and
though he begged his parents to come with, they refused; the last thing he heard from his family was [a censored postcard](http://www.straightdope.com/classics/a3_386.html) that read: You were right.

<div style="font-size: .8em">Updated 2014-02-13</div>

