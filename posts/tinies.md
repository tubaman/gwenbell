---
title: Why Tinies && Tech
date: 2014-01-12
---

I've communed with the tinies aka faeries aka fey for a long time. Lot longer than I admitted. Why? Because adults.

What about them? I've watched adults switch between dismissive: "oh, there's no such thing." And greedy: "You talk with faeries? Well, send them in to bail me out of my shitty life!"

Neither is good for faeries. And the latter in particular is very rip-up-the-tendrils-before-they-sprout-ish.

Anyway I don't talk _to_ them. I talk _with_ them or I just listen. Sometimes, if it's too loud out in the world, I retreat to a closet and shut the door.

Here's a photo I took in Japan of the kind of place faeries enjoy. Tinies enjoy fuzzies.

<img src="/images/green-fuzzy.jpg" class="profile" style="width:100%" />

Why am I writing about them now? Well, number of reasons. One, they keep going hey! We're here. Let them see us. 

And the other reason is [four locks](/fourlocks).

(The faeries are not renowned for their dealings with practical matters. I'm pretty sure they'd have zero practical matters if that were possible. Which is why I handle the practicalities for them. And [sometimes I screw up](/mistakes).)

Three, I think tech needs more faeries and play and less of the other stuff. If tech isn't playful and fun, why bother? And I'm not talking about mean misogynistic video games with billion dollar budgets. I mean actual play. Actual fun. Actual lightness.

If it weren't for the faeries I woulda left the world of tech a _long_ time ago. They feed me while tech SOPAs itself.

Anyway, I have lots of people to thank for acknowledging and supporting the tinies (plural for tiny). 

Tiny chief among them, [EB](http://evbogue.com). He loves the tinies and understands the care and feeding of them and has never, ever, not even once, tried to use them for his personal gain. Nor has he suggested I send them in to a dark place on a rescue mission from a disaster of his own creation. That's horrible for faeries, though they will help you out in a jam.

### 山前

If you can't see the above Japanese on your machine, [install Arch](/arch) and then pacman -Syu otf-ipafont uim.

The other person is Hashimoto Sensei. Who. Taught for years at the face of the mountain, which is what 山前 (Yamamae) means. Well really it means in front of the mountain. Hashimoto Sensei, it occurred to me as [Justin](http://justinsteimer.com) gave me a painting lesson. Hashimoto Sensei gave me my first painting lesson as an adult. He taught me the kanji for happiness 幸せ(Shiawase). He taught me to hold a brush. How to create brush strokes, long and lean.

I used to return to my desk after teaching a class to tiny origami critters. And invitations to tea. When his mom died he extended an invitation to me; I was the only non-Japanese human in the room that evening. Hashimoto sensei extended love love love. And I think it's in part because he acknowledged -- and cultivated -- his piece of the faery realm.

That's prelude. I just say all this to let you know, look out for them. My earliest sketches of daemons and kernels and grubs -- to demonstrate how systemd works -- are funny to me now, and it's only been a month since Justin got here and I started sketching for real. 

<div style="font-size: .8em">Updated 2014-02-05</div>


