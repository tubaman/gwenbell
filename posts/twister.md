---
title: Twister: Decentralized Social Networking
date: 2014-02-06
---

<img src="/images/twister.png" class="profile" style="width: 100%" />

After today's reports that [IRC](/irc) has been having some, **cough** government-induced **cough** technical difficulties, I logged on to [Twister](http://twister.net.co/) again. My blockchain was a few days out of sync, but that got rectified quick (under a minute).

If you're looking for up-to-the-moment statii, that's where I'll be. For now, I'm only twisting in English. If you want my Spanish updates, they're [here](/spanish). 

#### Why Twister?

It's distributed in a way that makes it similar to Bitcoin. It runs on a blockchain. That means each message is hashed to the previous and next one. Sound [familiar](/gitmagic)? That means? You guessed it. 

It's censorship-resistant. That's what we're after, right? (Because why bother saying anything anywhere you might be filtered out of the stream and not even know it? Exactly.)

#### Here's How

Here's how to get your Twister instance going.

1. [Download and follow the instructions here](http://twister.net.co/?page_id=23)
1. Join me in [IRC](/irc) if you have trouble, I'll help or someone else will, #gaman
1. Also, Twister has their own IRC channel, Freenode, #darkwallet

I know I have to watch myself to avoid addiction as the network grows, but right now it's the perfect size. And it's full of actual developers doing actual hard things with their lives. These are the kinds of people I spend my days with.

The brave ones.
