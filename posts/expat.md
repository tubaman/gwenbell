---
title: Expat
date: 2014-01-08
---

<mark>I'm an expat for life.</mark>

I've lived abroad more than half of my life. My first international flight was to Germany. 

<img src="/images/gwen-first-passport.jpg" class="profile" style="width: 50%" />

#### Places I Love

+ **The Yoop!**: I'll let [Nunu](http://nunubell.com) tell you all about it.
+ **Essaouira, Morocco**: when you get in the cab from Marrakech, be sure to pronounce the r firmly otherwise you'll end up in Essouaila like an idiot, and you will be hot and mad and not at the beach). I learned to surf in Essaouira. Jimi Hendrix did, too. Or, anyway. He played guitar there. Have the bread, hot off the stove, and olives
+ **Kyoto, Japan**: Skip the temples and shrines. Find the hippie van with the tent in the back
+ **Yokohama, Japan**: I opened my yoga studio and lived in Yokohama, near Motomachi
+ **Dingle Peninsula, Ireland**: Here there be faeries
+ **New York City**: Try Dough in Bed Stuy. If you can, visit the magical Schoolhouse in Bushwick. Also, Links in Manhattan
+ **Sevilla, Spain**: If you roll into Spain from Morocco (Tangiers), you'll take a water taxi type thing. The Moroccan crowd is not accustomed to traveling on water. Prepare for puke in the bathroom. Pack appropriate footwear
+ **Sayulita, Mexico**: Ask for Tigre on the beach if you want to learn SUP or how to surf. Ask for Jenn if you want a beautiful hand-made necklace. She'll probably be in Chiapas. If she's in town, she'll be with a curly-headed little girl named Luana Africa. Take tea tree oil with you. Have a Ciao Bella (espresso plus ice cream) at Chocobanana
+ **Nusa Lembongan, Bali**: This was the first time I snorkeled. I'm spoiled for life. If you visit, you'll see why
+ **Chuckanut Forest, Washington**: I've asked to have my remains released to the faeries here, where the water meets the treeline

***

At this point, everything I own fits into one bag. Though I used to pack two wheelies. So if you're not an extreme minimalist traveler, I feel ya.

A note on these recommendations. Though I did in the past for Nintendo, HP and a few other tech companies [mentioned here](http://www.fastcompany.com/1596274/gwen-bell-social-media-guru), I no longer do pay-to-recommend. Even then, I disclosed if I was getting paid. I recommend this stuff because it has improved my life.

#### Packing List

+ **Paints**: [Winsor & Newton Cotman Artist's Watercolor Travel Set](http://www.dickblick.com/products/winsor-and-newton-artists-watercolor-travel-set/). Used daily since January 2 2014, love so far. Sad to say mine didn't come with the tiny cute travel pouch! Only drawback: the paints rattle around in my Amy (see below.)
+ **Tech**: Light, tight [Linux stack](/tech). No phone.
+ **Notebook**: Palomino, by California Republic. Just finished my first one (started 9 September 2013, wrapped 14 December 2013). Beautiful notebook. Doubles as my daytime wallet (pocket in the back). Held up well with weeks of daily wear. I bought mine at [Press](http://www.pressworksonpaper.com/). Durable.
+ **Bag**: Mission Workshop. [Amazing bag. I travel with the Arkiv hardware](http://missionworkshop.com/products/advanced_projects/vx-rucksack.php). Elegant.
+ **Shoes**: [Minimus from NB](http://www.rei.com/product/845514/new-balance-mt10v2-minimus-trail-running-shoes-mens). A year plus in, they've molded themselves to my feet. Charmed.
+ **Mat**: I practice on a Manduka. Thin.
+ **Camera**: Leica D-Lux 5. Three years in, still use each week. Reliable.
+ **Warmth**: One black pashmina, one striped scarf, the [Amy Ibex](http://shop.ibex.com/Apparel/Womens-Skirts-Dresses/FT-Amy-Dress) <-- I love this dress, goes double now that I paint and write. Also have two pairs of Ibex hand warmers I wear tons, thanks [Nunu](http://nunubell.com). Warmed.

<div style="font-size: .8em">Updated 2014-02-05</div>
