---
title: How to Reach More People? Nut Graph.
date: 2013-12-13
---

<mark>Why are you telling me this? That's the question you have to answer if you want to reach more people.</mark> 

You have to answer the question, every day, every piece, "Why are you telling me this?" A nut graph is the answer to the question: Why are you telling me this? It could also be shortened: why should I care? 

If you're asking "how do I reach more people?" you're asking the wrong question. Why? Because "how do I reach more people?" is a series of questions rolled into that one non-question. 

Choose your favorite, more honest version of "How do I reach more people?"

+ Why do people click on my site and then instantly click away?
+ Why does nobody ever email me to ask a question?
+ Why, when someone does email me, do I either give a non-satisfying response, or give no response at all?
+ Why am I still on FaceCrack thinking that's "giving me reach"?
+ How do I listen so people want to talk?
+ How do I clean up my act so I can be present for another person when that person _does_ show up?
+ How do I reach _the one person_ engaging with my work right now?

<mark> How do I listen so people want to talk? </mark>

#### Why Are You Telling Me That?

The number one question you aren't answering in your work is, "Why are you telling me this?" 

That's why people click away from your site. That's why their ears turn off when you're having coffee with them. You never get to the nut graph. You never answer their one unspoken question: "Why are you telling me that?

The nut graph is the value in the piece you've published. The value to the reader. Not the value to you. The value to the reader. It answers the question, "Why are you telling me this? Why should I continue reading this piece?"

Without a nut graph to your piece, you lose your reader. Without a nut graph to your work, you lose the person engaging with it. Without a nut graph to your life, even _you_ probably lose interest in it. 

If you have no interest in [your own nut graph](/buyin), why would **I** have interest in it?

Answer, I wouldn't.

"What's the nut graph," is the number one question I ask [EB](http://evbogue.com). It is the number one question he asks me.

It is the number one question I answer in my work. If I'm not answering that question for you, I am failing as a writer.

#### Reach

How do you reach more people? 

The word _reach_ has been coopted, folded into so many other words, that it has lost all of its meat. 

I no longer say, "this'll help you extend your reach," because I think that's the wrong way to look at it. Reach is not a two-way street. Reach is what an octopus does with its tentacle. You are not an octopus. That's [a good thing](http://arstechnica.com/tech-policy/2013/12/new-us-spy-satellite-features-world-devouring-octopus/).

#### Don't Reach, Nut Graph

If you clean up your act, [align your website](http://align.gwenbell.com), learn [new tech skills](http://git.gwenbell.com) and take care of your digital self (Digital Warriorship), they will come. 

You won't have to "extend your reach" because one person becomes two when you do the work. 

And one person becomes zero when you're obssessed with extending your "reach" at all cost.

Don't reach.

Nut graph.

<div style="font-size: .8em">Updated 2013-12-21</div>
