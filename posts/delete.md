---
title: Delete
date: 2013-12-08
---

If you never hit delete, you never know what you have. To find out what you have, delete.

If you want to know how dependent you are on something, get locked out of it. It happened as I sipped on a coconut in Mexico. I got locked out of my Google account and realized my digital life was a wobbly, one-legged Google stool. That was December 2011. By December 2012 I'd deleted my Google account. This is how I got off and have been able to stay off, Google products.

#### Make Decisions

Ditching things requires you make a decision. A series of decisions, starting with processing your inbox. Process your inbox. Even with all the systems we've created to streamline our inboxes, we still fail to process them completely. To process it one final time, I went through my entire GMail box and made a decision about every message, stretching back to 2007.

Then I

+ moved any sensitive messages to a hard drive
+ had conversations with any people I needed to if there were things unresolved
+ responded, no matter how old the message was, and did not apologize for doing so

I was already using gwen at gwenbell.com. I was redirecting it to Google. I simply killed the redirect and as of December 2013 use [Mutt](/deliberate).

#### Actual Zero

After processing to actual zero, I took some time away from it. Just because it's now at actual zero doesn't mean you're ready to delete. I took three
months to delete my GMail account once I'd completely processed it. 

Then, I got out of my [filter bubble](http://dontbubble.us) by switching my searches to DuckDuckGo. 

Then, I removed myself from the Google cloud by migrating anything on Google Drive to my own external hard drive. 

I created a duplicate of that hard drive, so if I lose or damage one, I have a copy.

Now when I [write](/book) something, I use Git rather than the cloud-based Google Docs.

For a browser, I now use surf.

#### Leave Completely

When you leave the ecosystem, leave completely. Avoid sites that talk about Google, and stay away from sources you once enjoyed that are Google-centric.

The investment to do so is time, and paying closer attention. If you never hit delete, you never know what you have. What's left after you
hit delete? Just sit there and wonder.

Our work becomes more resilient through deletion.

