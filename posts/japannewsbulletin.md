---
title: Japan News Bulletin 1
date: 2003-07-11
---

<mark>Imported from LiveJournal, where I blogged from 2003 to 2007.</mark>

I know that no one is reading this yet, that I'm aware of anyway, but I want to take this time to let friends and family know that this morning I got confirmation that the visa process is going smoothly and the current leave date is September 3. It's possible that it will pushed to the end of August, depending. For those who are interested, the Nova Group website is <del>www.teachinjapan.com</del>. I know that there is a lot of bad press out there on Nova but I think, just like any job, you have to give it a fair chance. Dinner time.

<div style="font-size: .8em">Updated 2014-02-09 to delete website address; NOVA went bankrupt in October 2007. And I was stressed during my short tenure, but damn, [some people had it worse than me](https://en.wikipedia.org/wiki/Nova_(eikaiwa)).</div>

