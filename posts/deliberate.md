---
title: Sent From the Command Line
date: 2014-02-08
---

<img src="/images/mf-terminal-love-letter.jpg" class="profile" size="100%">

I can now sign all my outgoing messages 'sent from the command line' because I got [Mutt](/mutt) going last night. 

I saw Mutt ages ago but wasn't frustrated enough with Thunderbird yet to get it going. I hit that point earlier this year. 

Now, thanks to Mutt, I

	getmail

If you'd like to look at the documentation, [getmail](http://pyropus.ca/software/getmail/). (Python.)

Then I

	mutt

And it opens to my messages. 

Then I vim in to respond, hit send and it all happens from mutt to vim to mutt again. Then I'm out.

#### Deliberate Tech

Do you hear what I'm saying here? Technology the way I want it - on the receiving and sending side - is DELIBERATE. Not slower tech. Not less tech. Not even minimalist, though yes, I want it lean. I want, and therefore make, my digital environment in a deliberate way. 

Now you know when you get an email from me I didn't just 'pop it off' from my iPenis on my way to catch the next train.

It's possible to be both intentional and effective.

How?

Be deliberate.

There's something to it, isn't there? Being able to say: 'Sent from the command line.'

<div style="font-size: .8em">Published 2013-11-26. Updated 2014-02-08</div>

