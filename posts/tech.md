---
title: Stacks
date: 2014-02-09
---

<img src="/images/stacks.jpg" class="profile" style="width: 100%" />

It's not the size of the stack that matters. 

It's how you use it.

#### Paper Stack

+ Brush: Arches A4 Petit Gris Pur, France
+ Paper: Fabriano white 300g 50x60, Moleskine, China. I prefer California Republic, Palamino, California
+ Paint: Winsor & Newton, France
+ Pen: .30mm Sakura Micron 02 Archival Ink, Japan
+ Pencil: Bic #2 0.9mm, France

#### Tech Stack

+ Testing BitMessage: BM-2cUBURK3VQaVXEJiUMW8kZ22GrRZNA8kQ6
+ [Arch Linux](/arch): operating system
+ [Wicd: network connection manager](https://wiki.archlinux.org/index.php/Wicd)
+ [Hakyll](http://gitlab.com/gwenbell/gwenbell): gwenbell.com showcases the static site generator Hakyll, written in Haskell
+ [Reserva](http://evbogue.com/reserva): gwenbell.com features a lightweight CSS framework written by Ev, based on Skeleton
+ [surf](http://surf.suckless.org): distraction-less surfing
+ [DuckDuckGo](http://duckduckgo.com) search engine
+ [Mutt](/mutt): mail client
+ [xmonad](/approach): tiling window manager
+ [IRC](/irc) & [tmux](/tmux): digital communications
+ systemd: system/service manager
+ [Git](/gitmagic): distributed version control
+ Vim: text editor
+ zsh: shell
+ [Gitlab](http://gitlab.com/u/gwenbell): repo management
+ mplayer, alsamixer, bluetoothctl for Auvio: music 
+ [GIMP](/gimp): graphics
+ Acer Aspire S3: hardware
+ [FetchApp](http://app.fetchapp.com/signup?ref=gwenbell) (rewards link): easiest way I've found to sell digital products and services
+ [Digital Ocean](https://www.digitalocean.com/?refcode=14a7846491d4) (rewards link): $5/month or less VPS

<div style="font-size: .8em">Updated 2014-02-10</div>

