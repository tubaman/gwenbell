---
title: Zed Shaw's Stack
date: 2014-02-10
---

<img src="/images/zedshaw.jpg" align="right" class="profile" style="width: 100%; margin-bottom: 1em;" />

I'm crushing on Zed Shaw's [Stack](/tech).

Who's Zed? 

Well. [He's a programmer. Writer. Guitarist. Artist. Former military. Rockstar.](http://learncodethehardway.org/). 

Why'd I interview him? At the start of 2014 I committed to only publishing things that [turn me on](/turnon), and at the top of the list was this interview.

Zed you can either [give yourself over](/compersion) to loving and learning from, or you can float your jealous ass on down the river. I doubt he'll care either way. If after listening to the interview you _do_ fall in love with the work Zed does, know this. You are not alone.

People of the internet, I give you. Maybe you'll learn a thing or two.

**Zed Shaw's Stacks**

<audio controls="controls">
   <source src="http://sounds.gwenbell.com/zedshawstack.mp3" type="audio/mp3"/>
   <source src="http://sounds.gwenbell.com/zedshawstack.ogg" type="audio/ogg"/>
</audio>

<mark>Disclaimer: if you don't see the audio above, try a modern browser. Or download here: [mp3](http://sounds.gwenbell.com/zedshawstack.mp3) | [ogg](http://sounds.gwenbell.com/zedshawstack.ogg)</mark>

### Interview Notes

#### Foundation

+ Evolving, flexible stack, always changing it up
+ Can do programming on any machine in existence
+ Vim, macros to carve through code fast; Vim's a powerful scritable editor
+ Git, Subversion, any revision control tool
+ Shell, bash, not too particular
+ Screen on any system (similar to [tmux](/tmux))
+ [Screen](https://www.gnu.org/software/screen/) to run servers

#### Languages

+ Python: quick web stuff
+ Django
+ Zed's own brew: [J-Zed](https://github.com/zedshaw/jzed)
+ jQuery 'cuz it's there

#### Other Tools

+ Objective-C
+ Next (pre-Apple)
+ writes a lot of automation
+ HTML/CSS

#### Automate

+ tests
+ system reloads
+ deployments

#### For Writing

+ reduce the friction
+ Vim
+ [Dexy.it](http://dexy.it): book generating system, runs code, run browsers, grab screenshots
+ Also, generate website with Dexxy

#### Boxes

+ Simple, simple, simple
+ Mac (when lazy) 
+ Linux
+ Windows to make sure know how

> Tools are just tools, I don't fetishize or obssess over them.

#### Where do you write?

+ Plastic garden shed sound booth, very cheap
+ Guitars all over the walls
+ Art, learning art for future book
+ Creative, relaxing, toys
+ Coupla nights, slept here "just because I had to"

#### What do you listen to when you work?

+ Jazz
+ Miles Davis
+ Grant Green
+ Spoke Mathambo (high energy music) 
+ Whatever's playing in head
+ When an album ends, get up, take break
+ Brain starter: Just Blues, Miles Davis

#### Frustrations with your stack?

> I'm not a pansy who has to code on a Mac.

+ Wants to switch coding and writing to the Linux machine
+ Mac for graphics/audio
+ Mac Mini for Internet TV
+ Good at optimizing, which can slow things down
+ Optimizing ability to complete something before actually completing it

#### If you wrote your own operating system, how would it operate?

+ [MINIX](http://www.amazon.com/Operating-Systems-Design-Implementation-Edition/dp/0131429388)
+ New, small operating systems coming out
+ Internet of Things
+ Minimalist operating systems with few GUIs/Graphics
+ Minimalist but powerful
+ Purpose-focused operating systems

#### How'd you time in the military impact your work now?

> I learned to shove my personal bullshit to the side and get shit done. 

+ Why I'm not a victim of fads and propaganda as much
+ Evaulate as objectively as possible
+ Putting in effort and work hard, you can learn just about anything

#### You've cooled the hot-headed pieces, why?

+ 2008. Trying to correct course of young male programmers
+ Learn Python the Hard Way: make better programmers
+ Make savvier, less douchebaggy programmers
+ Teaching to code in a gentle way, rather than rants
+ Rants: audio/video is better
+ Learned to sing/do voice work for better rants
+ Conference talks: rants turned into performance art
+ The next phase
+ People pretend their offended to do indirect slandering
+ Still pretty offensive
+ More that it doesn't fit current feelings about how to improve the world
+ Gentle but firm instructional books
+ Creative energies: music and creative writing

#### Have you ever met your tech match? How will you know when you have?

+ Anything worth learning is difficult
+ If it's easy, it's not that advanced or you're not pushing yourself
+ Learned from martial arts: there's always someone better than you, so don't be a dick, there's always someone who can kick your ass
+ The tech industry takes really smart people and grinds them out
+ So they end up not doing anything creative or original
+ ZS: takes oblique/long angles
+ Create things that are simple/are me, but still have large amount of impact
+ webservers
+ books
+ rants
+ silly little t-shirt
+ Minimal effort, large impact
+ Most of Learn Python the Hard Way written while watching tv
+ ZS: Produce simplicty with very little effort and have it be very powerful
+ Different taste perception for what programming should be
+ Boiling down problem down to exactly what's needed and just produce _that_
+ Can't recall anyone that's like that
+ Former awesome heroes get freaked when ZS does what ZS does
+ People in tech world aren't playing in the same game

<div style="font-size: .8em">Updated 2014-02-10</div>

