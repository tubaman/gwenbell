---
title: A Repository of Forks and Other Uncomfortable Going Ons with Pablo Caro
date: 2013-12-18
---

Pablo Caro emailed me last week to say he'd cloned my site off [GitLab](http://gitlab.com/gwenbell/gwenbell). I took it as a chance to ask him to tell me about himself.

> Hi there, I'm Pablo Caro. A link on your site guided me to your gwenbell repo on GitLab. I haven't used GitLab and I have no experience with it, although I signed up some days ago. Once there, I saw a button labeled as "Fork repository". For some reason, instead of reading "button that makes a fork of the repository", my brain decided that the actual meaning was "Repository of forks". Extremely curious about that mysterious repository of forks, I clicked.

**GB:** Are you rebuilding [your site right now](http://pcaro.es)? Holler when you're ready to unveil it.

**PC:** To tell you the truth, I don't know if it's done. I'm not even sure
where is it going. The only reason I did it is that I have finished a
period of my life when everything was comfortable and nice, and I
realized I didn't have a plan after that. So I'm just trying to push
myself out of my comfort zone, without thinking it too much, in order
to stop being that cozy. At least until I decide what to do now.

I'm not a people person and I tend to be very introvert, so I started
sharing my thoughts and my experiences. I don't feel comfortable
writing in English, so I did it in English. I don't even feel
comfortable telling people about all this, and here we are. Hey,
Gaman, I guess.

So, my site... Well, I think it's ready. It's not going to be readier,
anyway.

**GB:** I love that each thing you found you were uncomfortable with, you did. Last night I was talking with someone, his name on IRC is davuxx, and he pushed me to learn how to add accent marks to what we were talking about in Spanish. So he started teaching me, in Spanish, new Linux commands. I felt so frustrated with myself I wanted to hurl my machine across the room.

And you know what? Once I figured out how to Compose with AltGr, and did my first upside down exclamation mark, I felt accomplished. So yes to doing hard things. [Gaman](/gaman).

(Care if I share this?)

**PC:** Sure thing, feel free to share it. I hope it pushes someone else to do
uncomfortable things. In the good sense, you know.

Por suerte, el español no tiene demasiadas cosas raras. ¡Y es un
idioma precioso! Muchísima suerte con ello.
