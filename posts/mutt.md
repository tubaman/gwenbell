---
title: Mutt, The Homely Email Client
date: 2014-02-09
---

If you want to know how reliant you are on something, get locked out of it.

In December 2011 I lived in Sayulita Mexico. More on that [here](/expat).


### How I Got Locked Out

I'd turned on 2-step verification for my GMail account. I'd had that account for years and I didn't want someone to hack in and steal all my vital bits. And I was living abroad and Google kept saying, "2-step! 2-STEP!". So, two-step I did.

Then, I had to get back in with 2-step. The only phone I had access to was my Google Voice account. Guess what? If you used Google Voice as your phone in late 2012, tGoogle didn't view that as an acceptable phone number with which to verify your identity.

For less than 168 hours but more than 72, I was **locked out** of my email account.

Now, I know some people in tech. Not a ton. But enough to ask real nice and get let back in. Someone let me back in. But I'd already seen I had a problem. Or several problems.

+ I'd entrusted all my important bits to Google
+ I went: Gwen, if you're afraid of being hacked, why aren't you also afraid of what happens to your important bits at Google?
+ I saw there was actually nothing all that important in my inbox worth fretting over getting stolen
+ I realized most of what was going on with my inbox was I was self-soothing with it: I exist, I exist, I exist. My massive inbox number/contact number proves it!

I got off. But it took another year. 

#### How to Get off GMail

Here's what I did over that year.

+ I processed my entire inbox
+ I looked at each message and made a decision either to delete or respond
+ Even if something was seven years old, I still looked at it
+ In that way, I cleaned out the wounds, settled old beefs and apologized for shitty loops I hadn't closed
+ Then, I closed my Google account

#### How to Get onto Mutt

Mutt is [a command line tool](/approach). Command line means no graphical user interface. That means no red or blue or sparkly fireworks notifications when you have an incoming message. You do 

	getmail

and then pause for a moment. If there's a new message do

	mutt

and read the message. All from the command line. [Deliberate](/deliberate) not addictive.

The best tutorial I've seen, and the one I used to get set up is Steve Losh's [The Homely Mutt](http://stevelosh.com/blog/2012/10/the-homely-mutt/).

All told, getting off GMail took about a year. The processing of the inbox took about a month. And setting up Mutt took about half a day.

If you decide to migrate, rock on. I think it's [brave](/brave) of you to give it a go.

If you really wanna know why I left? It wasn't just the lock out. 

I decided I'd given enough of myself to a corporation from which I was no longer benefitting. No longer getting [nutrients](/nutrients).

<div style="font-size: .8em">Updated 2014-02-08</div>

