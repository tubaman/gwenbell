---
title: Contact
date: 2014-02-14
---

+ [IRC](/irc): Freenode, #gaman
+ BitMessage: BM-2cUBURK3VQaVXEJiUMW8kZ22GrRZNA8kQ6
+ Email: gwen at gwenbell.com
+ Twister: gwenbell
+ In person: contact for details

***

I maintain a [hand-crafted](/handcraftedlist). To sign up, send an <a href="mailto:gwen@gwenbell.com">email</a> or bitmessage me your bitmessage address and I'll add it to group messages.

