---
title: 5 Questions for Gwen Bell: On Tech and Infinity
date: 2013-12-21
---

You might have read my interview with [Roslynn](/roslynntellvik). Now I'm answering the questions she sent my way. Reciprocity.

**RT:** What about the work you are doing right now resonates for you?

**GB:** Right now I'm working on a new book. I'm coming into the final stages. That's always an exhausting and exciting time. Exhausting because I leave it all on the field each day. Exciting because I love bringing new books into the world.

This one is a non-technical guide. It's still a guide, becase guiding's what I do, but it's not _as_ technical as [Align](http://align.gwenbell.com) or [GC](http://git.gwenbell.com). I'm publishing it in part because I've gotten dozens of requests in the past few months for something "less technical" and because when I know I have to publish something, I do. 

You asked what resonates about my work for me. That's not a question I ask myself. 

I ask myself whether I'm getting [the nutrients I need](/nutrients). The answer to that question is yes. The answer to that question, for my work to land, _has to be yes._

**RT:** What sparked your passion for tech?

**GB:** I made my first stabs at a website in 2001 or 2002. I was in college. 

It was an English class I hated. That was rare. In fact, I can't remember what course it was, I hated it that bad. The fact we were being, in my mind, forced to make a website took the pleasure right out of it. I did a shitty job to get a pass, but there was no love there.

Side note: anytime I sense I'm being forced to do something, I dig my heels in and refuse to do it. Lifelong pattern I don't try to break. Make requests, not demands, and you'll find we have a better time.

2003 I flew from North Carolina to Tokyo. Before that I talked to this guy Brian Rice, a barista at [Driade](http://caffedriade.com/), a cafe I'd spent hundreds of hours in. I'd heard about weblogs. Brian suggested LiveJournal. I [dedicated my first post to him](http://gwendolynbell.livejournal.com/477.html). 

I'd become a blogger.

If it's not clear by now, I started this journey of writing to the web not because I wanted to get my mug on a magazine cover, speak all over the globe and get wined and dined by big wigs and wannabes. 

Negatron. I did it because I wanted to correspond [with Nunu](http://nunubell.com). 

She was one of my earliest commenters back when I allowed comments. She reads my work now, this many years later. She read my work as a kid. Far as I know she only read my private journals once or twice when she thought I might be suicidal or something. 

She always encouraged my writerly tendencies, whether analog or digital. If she wasn't encouraging, she was staying out of my way.

Technology, at its best, greases the wheels between humans.

All that other stuff, secondary. I don't have "a passion" for technology. I am interested in humans, and technology is part of the path to getting at humans.

**RT:** Once upon a future time, what technological or other life changing innovations would you like to see?

**GB:** I don't give a damn what happens next in tech. I'd like to see humans put down their phones when they're driving. I'm eager to see them put down their devices when they're walking around. Ideally, I want to see them get rid of their phones since we now know they're tracking our every move.

I'd like to see humans get off the addictive social networks upon which they're codependent. Anne Wilson Schaef's _When Society Becomes an Addict_ is a good place to start. When I took my first digital sabbatical in 2010, I paper tweeted. I am now off Twitter entirely, as I believe it, too, has become dangerous.

A pen and a piece of paper, when you're getting clean, does the trick.

So it's not some fancy gadget I wish would be invented. It's humans considering their relationships to technology we need more of.

**RT:** Why do you include the uninitiated folks like me in your technical assistance work?

**GB:** That's the work. That's _my_ work. 

I started programming the houses in which my work now lives, not because I wanted to be a programmer changing the world. I'm already a writer changing the world. I wanted to house my work in the correct way. I wanted to learn my entire stack because the person who cares most about that stack is me. 

So now I am on a [Linux box](/arch), writing on the command line, single-tasking because I was going crazy living in a house -- if Apple operating systems can be called houses -- let's call them expensive, well-polished shanties. 

Anyway, I was going crazy in the shanty called Apple. So I [left](/leave). It was 2012.

Now it's nearly 2014. The house I built - am always building - with help from the likes of Linus Torvalds, Richard Stallman, Davuxx, Mike Wong, Ev Bogue and the countless other dudes (always dudes) willing to throw down with me - matters to me. And as I said before, [to you, Roslynn, the only person who needs to buy into this process is me](/buyin).

**RT:** What have you noticed about the interplay (or contrast) between the experience or practice of digital presence and physical presence this year?

**GB:** If my physical self is healthy, well and self-reliant, my digital self ought to be, too. It's a process of alignment, always. What worked last month may not work this month. When I see a practice or tool that makes sense to incorporate, I do. 

It's a tiny infinity cycle, this work.

Speaking of, I have a book to wrap. 

***

Thanks for the questions, Roslynn. _Ganbatte_ with [your new business](/roslynntellvik).

g
