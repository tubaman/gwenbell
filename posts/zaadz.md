---
title: Zaadz Self-introduction
date: 2006-05-17
---

<mark>I added this piece on 24 January 2014. It's from 2006, posted to a site I did freelance web design work for, Zaadz. Zaadz no longer exists, but I kept some of those pieces.</mark>

(Posted to Zaadz May 17, 2006)

Hi everyone,

This is Gwen. I'm new here and wanted to say hello and introduce myself briefly.

I'm originally from the States but have lived in Japan the past three years and am now in the business of building _sangha_ through yoga, weekly meditation sessions at the studio offering health/healing resource info to the community!

Whew. The studio website is: Yoga Garden (update from Gwen: the website is no longer) and I build that, too. The studio is a vision-in-progress and a goal realized for me. I opened it when I was 23 and in the year since it opened have grown so much with it. Even the studio's motto, "grow with it" reflects my life-philosophy. So, you can do it. Every one of you! And that's why I'm at Zaadz, to uplift, encourage! To plant and water seeds!

I have a long-term and longer-term project that I'd like to share with you all. The long-term project is to build the [Gwendolyn Round House](/regenerate). This is my first time to publicly share this, so it's going to take a moment to explain. The concept is simple. I'd like the house to be round, with no corners, the base is round, like a circle, and the walls are curved and go straight up (the roof is covered with solar paneling and windows to let in natural
light and how I'm going to curve them is still being thought-out). Not like the Epcot center or even a <a title="GDH" href="http://www.naturalspacesdomes.com/" target="_blank"> geodesic dome home </a> , although the GDH will give you an idea of what I'd like to create.

I'd like the house to be built with the principles laid out in _Voluntary Simplicity_ by Duane Elgin. Basically, the inside will not have doors to divide rooms, but moveable partitions, _a la_ Japanese sliding _shoji_. The energy in the house will move freely, no stuck feeling.

The long-long term project is to create a community based on ecologically, environmentally sound wholesome living principles FROM THE GROUND UP. Imagine your local whole foods, farmer's markets, acupuncturists, eco-sound clothing stores, schools built on progressive learning principles, all in one place, you can walk easily from place to place with no need for a car. I know, it sounds like an ambitious project. Obviously I know this will mean many, many people on board, VCs and
Angel Investors and eco-architects, but I believe this can happen and want to be a part of it. I've been adding to the vision over the years and now here I am sharing it with you. It's a life-long project.

I did my studying at the University of NC in Chapel Hill. Anyone that's ever been to Chapel Hill, or more accurately, Carrboro understands the kind of community I'd like to create and be a part of. It'd basically be Zaadz, Come to Life. Hmm. Sounds wonderful.

Well, that's me! What about you!?
