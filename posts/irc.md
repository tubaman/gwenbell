---
title: IRC
date: 2013-01-27
---

> Twitter needs to decentralize or it will die. - al3x, one of Twitter's earliest engineers

Twitter IPO'd today, and I feel responsible. I see now the importance of helping you at least be aware of, if not help you onto, something better. A protocol, which is what Twitter could have been but [did not](http://al3x.net/2010/09/15/last-thing-about-twitter.html) become.

#### IRC is It

It sounds neck beard nerdy to even be talking about. But now that the IPO has happened, I've put Twitter on my [Deemed Harmful list](/tech). Because if we're to have a [revolution](https://en.wikipedia.org/wiki/Internet_Relay_Chat#History), we have to have a way to talk that's non-censorable. IRC is it.

(By the way, I assume all of IRC is recorded, even if it's not censored.)

As Malcom Gladwell [put it in 2010](http://www.newyorker.com/reporting/2010/10/04/101004fa_fact_gladwell) -- the revolution will not be tweeted.

> Decentralization isn't just a better architecture, it's an architecture that resists censorship and the corrupting influences of capital and marketing. - al3x

So, what's IRC? It's internet relay chat. It's a protocol.

#### How to Use IRC

You get a client. I use [irssi](https://en.wikipedia.org/wiki/Irssi). Then, you choose a username and go into a room.

It takes a little time to get it all set up. But once you [gaman](/gaman), you'll find there are thousand of active channels, some of which you won't learn about until you've been using IRC for a while. There's no public, centralized list of IRC channels and there are channels you have to be invited into. 

Once you're in a room (I'm in #gaman on Freenode) you simply speak into the room.

It's a little how you might have felt if you were on Twitter in 2007. 

You, an empty room, just writing onto the wall, having no idea if anyone hears you. Though if you come into #gaman right now, in December 2013, there will be others there with you.

If you want to take it to the next level, use [tmux](/tmux) - a [terminal multiplexer](http://www.openbsd.org/cgi-bin/man.cgi?query=tmux&sektion=1). You get messages even if you're not in the room when they're sent. A la those little white boards you use in college to let your roomie know you're out of the room for a day or two so she can have some heavy petting sessions in your absence.

#### To Register Your Username

I was just in IRC with someone who got a six letter username somehow, by accident. If that happens to you, register that shit.

To claim that username as your own, enter freenode and the #gaman channel where we are kind to n00bs,

Then, you gotta add a password

	/msg NickServ register <password> <email>

Be aware, your password is stored in plaintext. Don't use a password you use for say, your online banking account. (This goes without saying, don't reuse passwords, but here, it's uber important: use a throwaway password that you wouldn't mind someone glancing by accident.)

You're all set up. Now, you have your six character username and a password to boot. That way, that username is yours. And if it's only six characters long, consider yourself lucksters.

Also, if someone else is already using it, it'll show up in another window telling you so!

#### Why Use IRC

Ok, so if it's not clear at this point, let me be clear:

1. Twitter is now a publicly traded company. Meaning it answers to share-holders
1. I assume share-holders (of which I am not one) have profit-driven goals
1. I don't care what their goals are; I assume they'll go to any lengths to drive share prices up
1. While I don't begrudge those who make money from this IPO, I'm interested in decentralized ways for people to communicate; Twitter's not it
1. Along the lines of [Git and GitHub](/gitnotgithub) not being the same, Twitter and our ability to communicate are not synonymous
1. IRC has been around since the 80s, and it's not going anywhere, it's not possible to censor it, and it's not IPOable

It's a challenge to learn (was for me, anyway) but now that I've deemed Twitter harmful, that's where you'll find me. Leave me a message if you come in and I'm not there; I'll check it. I have tmux running.

As for Twitter, we had some good times. 

Maybe I have the service to thank in part for me being where I am now, a programmer and writer. After Twitter, I'd seen the full range of human expression and possibly human potential. And with a few exceptions, nobody blew me away. One of those exceptions woke up in my bed this morning. And without Twitter, pre-TWTR, could this ever have been? I mean, probably. But actually?

[Send questions](mailto:gwen@gwenbell.com) or pop into #gaman if there's some way I can make this tutorial stronger. It takes some practice.

<div style="font-size: .8em">Updated 27 January 2014</div>

