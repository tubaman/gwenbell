---
title: Gwen Bell Joel Longtine Divorce
date: 2013-04-19
---

<img src="/images/gwen-joel-pre-divorce.jpeg" class="profile" style="width: 100%" />

In 2007 while I was working at TechStars I met this programmer. He's now a former programmer. 

His name is Joel Longtine.

After his company was acquired we got married. It was 2009. The marriage lasted a year, not even. By 2010 I had moved into my own place, The Urban Retreat. I met Ev Bogue. Joel choked him, and wouldn't let go. Joel was drunk at the time. I was sober. Ev was sober.

The cops came and threw Joel in the back of a cop car, double restrained.

The next morning I lost the lease on The Urban Retreat. I threw all my shit in a trash can, packed my one bag and moved to New York City. It took another year to get a divorce. I wouldn't recommend getting married. 

It's a pain in the ass to get unmarried.

The end.

***

"No way. You got a divorce! But you're so young!"

I laughed. I told her she can ask me anything. I'm ready to talk about it now.

I initiated the divorce while he was in jail. He was in jail, and there was a restraining order out against him, created by the state to protect me.

Boundaries.

Sometimes the state lends a hand with that.

While he was in jail, I told him I was divorcing him and then we didn't talk for weeks. It would have been illegal for him to contact me. It's called a no contact.

I flew to New York. I started writing to the paid letter. I did yoga. I drank. And that's about all I did. Though I sought refuge, I didn't find New York stabilizing. The upshot was I found myself suddenly surrounded by others with their own fierce lives. Photographers, models, heavy smokers who like
drum and bass.

And that, yeah. Helped in its own weird way.

That was two years ago this month.

I write this letter to celebrate. It has taken two years to write to you about it, and now I can.

When it happened, I went dark. I [killed](/delete) social profiles to get space and teach the algorithms we were no longer connected. 

That created the Streisand Effect. When you're as public and enthusiastic about your life as I am mine, well. Of course there was curiosity. Odd thing, hardly anyone contacted me directly about it. When I did reach out to internet friends I knew AFK, I was met with silence. I'm still a little tender around that.

And. I know they have their own things going on. As when mom died, I learned we deal with suffering in each our own way.

The way I deal is I go back out there, tender. Knowing the reason I have a heart is to have it broken open, again and again.

How else would flowers bloom?

Gwen <br />
19 April 2013 <br />
Brooklyn, New York <br />

**Two years prior**

It's Spring in New York City. This morning, a rain hammered the fire escape outside my window.

I have stories to tell you. Believe me, I want to tell you everything. Right now, I can only tell you snapshots. I can only tell elements of the experience; I hope you'll understand.

This isn't reticence to share - things changed rapidly in a 12 hour period in April 2011. Following a digital blank slate (in February) my life was also blank slated. It was unexpected (as most things that change things are) and I'm unsure what's next.

What's certain is this: the life events I've shared with you in the past (mom's death, the house burning down, international moves) - I've just experienced another event of that scale. I am realigning not just the site now, now just the digital presence, but the offline work, too.

//

This is a slow unfolding, an unfurling. I'm writing more than I've ever written. Much of it goes to the letter, some stays in the notebook. Some goes on an index card to be hand-shredded.

As my life unfolds the intensity of living simply has brought me to a place of great clarity with my work. I'm taking fewer clients, but better matches. I'm speaking less - and only at engagements with which I resonate - including Blogher this summer.

I'm working with teams (including Language Dept. here in NYC) whose work I grow alongside my own.

//

Increasingly I am aware of the finitude of all this - this life, this site, this body, yours. I'm aware of the ranunculus blooming during long walks in the city. As the work deepens I invite you along, knowing it's practice, all of it.

Gwen <br />
28 April 2011 <br />
Bread, Manhattan, NYC <br />
