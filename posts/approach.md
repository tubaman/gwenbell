---
title: Approaching the Command Line
date: 2014-02-07
---

<video src="https://f92fac806bf10a96c0b8-8a0a46e5f1a5cc9854958bc3503f0f88.ssl.cf1.rackcdn.com/media_entries/6701/approach.webm" width="640" height="360" controls></video>

I run my business from the command line. I use very few GUIs, and those I do use I try to keep free/libre/open source, such as [GIMP](/gimp).

I made this vid, Approaching the Command Line, so you get a glimpse of _how_ I run my business from the command line. 

I encoded the video webm. It's MediaGoblin (Python) hosted-- [you can view it there, too](http://gobblin.se/u/gwenbell/panel/). 

You're looking at [xmonad](http://xmonad.org). More on encoding from the command line -- [ogv to webm](http://paulrouget.com/e/converttohtml5video/).

The last time I did a screencast was at the start of 2013, a year ago. I was still on a Mac. I was still on Ubuntu. I had just wrapped my head around [Git](http://git.gwenbell.com). 

I hadn't learned Node, let alone started in on Haskell. Just a year later, I'm all Arch, xmonad, Gittastic. 

A lot can happen in a year. What will happen in yours!? 350 something days left in this one.

Plus! TIL "Monad (from Greek μονάς monas, "unit" from μόνος monos, "alone"), according to the Pythagoreans, was a term for Divinity or the first being, or the totality of all beings, Monad being the source or the One meaning without division." [Source](http://en.wikipedia.org/wiki/Monad_(philosophy))

<div style="font-size: .8em">Published 2014-01-17. Updated 2014-02-08 to flesh out.</div>

